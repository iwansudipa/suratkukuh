<page backtop="10mm" backbottom="10mm" backleft="20mm" backright="20mm">
<table style="width: 100%">
<tr>
    <td style="width: 10%"><img src="../../../img/kab-lotim.png" alt="" width="80"></td>
    <td style="text-align: center; width:80%"><b><span style="font-size:15pt">PEMERINTAH KABUPATEN LOMBOK TIMUR KECAMATAN PRINGGABAYA</span><br>
    <span style="font-size:20pt">DESA BATUYANG</span><br><br><span style="font-size:10pt">Jln Raya Jurusan Mataram-Batuyang kode Pos 83654</span> 
    </b></td>
</tr>
</table>
<hr border="2">
<p style="text-align: center; font-size:14pt;"><b><u>SURAT KETERANGAN CATATAN KEPOLISIAN</u></b><br>
<span style="font-size:12pt">Nomor : <?php echo $data['no_suratkeluar'];?></span></p>
<span style="text-align:justify;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Yang bertanda tangan di bawah ini Kepala Desa Batuyang Kecamatan Pringgabaya Kabupaten Lombok Timur, 
menerangkan dengan sebenarnya Kepada :</span><br><br>
<table style="width:100%" border="" align="center">
<tr>
    <td style="width: 8%;"></td>
    <td style="width: 30%;">1. Nama</td>
    <td style="width: 1%;">:</td>
    <td style="width: 61%;"><?php echo $data['nama']?></td>
</tr>
<tr>
    <td style="width: 8%;"></td>
    <td style="width: 30%;">2. Tempat/Tgl. Lahir</td>
    <td style="width: 1%;">:</td>
    <td style="width: 61%;"><?php echo $data['tempatlahir']?>, <?php echo date('d-m-Y', strtotime($data['tgllahir']))?></td>
</tr>
<tr>
    <td style="width: 8%;"></td>
    <td style="width: 30%;">3. Jenis Kelamin</td>
    <td style="width: 1%;">:</td>
    <td style="width: 61%;"><?php echo $data['jk']?></td>
</tr>
<tr>
    <td style="width: 8%;"></td>
    <td style="width: 30%;">4. Agama</td>
    <td style="width: 1%;">:</td>
    <td style="width: 61%;"><?php echo $data['agama']?></td>
</tr>
<tr>
    <td style="width: 8%;"></td>
    <td style="width: 30%;">5. Status</td>
    <td style="width: 1%;">:</td>
    <td style="width: 61%;"><?php echo $data['status']?></td>
</tr>
<tr>
    <td style="width: 8%;"></td>
    <td style="width: 30%;">6. Pekerjaan</td>
    <td style="width: 1%;">:</td>
    <td style="width: 61%;"><?php echo $data['pekerjaan']?></td>
</tr>
<tr>
    <td style="width: 8%;"></td>
    <td style="width: 30%;">7. NIK</td>
    <td style="width: 1%;">:</td>
    <td style="width: 64%;"><?php echo $data['nik']?></td>
</tr>
<tr>
    <td style="width: 8%;"></td>
    <td style="width: 30%;" valign="top">8. Alamat</td>
    <td style="width: 1%;" valign="top">:</td>
    <td style="width: 61%;">Dusun <?php echo $data['dusun']?> RT/RW <?php echo $data['rt']?> Desa <?php echo $data['kel']?> Kecamatan <?php echo $data['kec']?> Kabupaten Lombok Timur</td>
</tr>
</table>
<br>
<span style="text-align:justify">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Dengan ini menerangkan bahwa berdasarkan data dan catatan yang ada serta sepanjang 
pengetahuan kami tentang orang tersebut, memang benar :</span>
<br>
<table style="width:100%" border="" align="center">
<tr>
    <td style="width: 8%;"></td>
    <td style="width: 30%;">1. Penduduk warga Desa </td>
    <td style="width: 62%;">: Batuyang</td>
</tr>
<tr>
    <td style="width: 8%;"></td>
    <td style="width: 30%;">2. Kecamatan</td>
    <td style="width: 62%;">: Pringgabaya</td>
</tr>
<tr>
    <td style="width: 8%;"></td>
    <td style="width: 92%;" colspan="2">3. Berkelakuan baik di dalam bermasyarakat</td>
</tr>
<tr>
    <td style="width: 8%;"></td>
    <td style="width: 92%;" colspan="2">4. Tidak pernah tersangkut perkara kriminal dengan instansi Kepolisian</td>
</tr>
<tr>
    <td style="width: 8%;"></td>
    <td style="width: 92%;" colspan="2">5. Tidak dalam status tahanan yang berwajib</td>
</tr>
<tr>
    <td style="width: 8%;"></td>
    <td style="width: 92%;" colspan="2">6. Tidak pernah terlibat dalam penggunaan obat-obat terlarang</td>
</tr>
<tr>
    <td style="width: 8%;"></td>
    <td style="width: 92%;" colspan="2">7. Surat keterangan ini dibeikan untuk keperluan :  <b><?php echo $data['keperluan']?></b> </td>
</tr>
</table>
<span style="text-align:justify">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Demikian Surat Keterangan Catatan Kepolisian  ini dibuat dengan sebenarnya untuk dapat dipergunakan sebagaimana mestinya.</span>
<br><br><br><br><br><br>
<table style="width: 100%" align="right">
<tr>
    <td style="width: 30%; text-align: center">Batuyang, <?php echo date('d F Y');?> 
    <br>Kepala  Desa  Batuyang	
    <br>
    <br>
    <br>
    <br>
    <br>
    <b><u>Drs. SYARAFUDDIN</u></b></td>
</tr>
</table>
</page>