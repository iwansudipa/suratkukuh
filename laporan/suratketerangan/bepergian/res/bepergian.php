<page backtop="10mm" backbottom="10mm" backleft="20mm" backright="20mm">
<table style="width: 100%">
<tr>
    <td style="width: 10%"><img src="../../../img/kab-lotim.png" alt="" width="80"></td>
    <td style="text-align: center; width:80%"><b><span style="font-size:15pt">PEMERINTAH KABUPATEN LOMBOK TIMUR KECAMATAN PRINGGABAYA</span><br>
    <span style="font-size:20pt">DESA BATUYANG</span><br><br><span style="font-size:10pt">Jln Raya Jurusan Mataram-Batuyang kode Pos 83654</span> 
    </b></td>
</tr>
</table>
<hr border="2">
<p style="text-align: center; font-size:14pt;"><b><u>SURAT KETERANGAN BEPERGIAN</u></b><br>
<span style="font-size:12pt">Nomor : <?php echo $data['no_suratkeluar'];?></span></p>
<span style="text-align:justify;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Yang bertanda tangan di bawah ini Kepala Desa Batuyang Kecamatan Pringgabaya Kabupaten Lombok Timur, 
menerangkan dengan sebenarnya Kepada :</span><br><br>
<table style="width:100%" border="" align="center">
<tr>
    <td style="width: 8%;"></td>
    <td style="width: 30%;">1. Nama</td>
    <td style="width: 1%;">:</td>
    <td style="width: 61%;"><b><?php echo $data['nama']?></b></td>
</tr>
<tr>
    <td style="width: 8%;"></td>
    <td style="width: 30%;">2. Tempat/Tgl. Lahir</td>
    <td style="width: 1%;">:</td>
    <td style="width: 61%;"><?php echo $data['tempatlahir']?>, <?php echo date('d-m-Y', strtotime($data['tgllahir']))?></td>
</tr>
<tr>
    <td style="width: 8%;"></td>
    <td style="width: 30%;">3. Jenis Kelamin</td>
    <td style="width: 1%;">:</td>
    <td style="width: 61%;"><?php echo $data['jk']?></td>
</tr>
<tr>
    <td style="width: 8%;"></td>
    <td style="width: 30%;">4. Agama/Kewarganegaraan</td>
    <td style="width: 1%;">:</td>
    <td style="width: 61%;"><?php echo $data['agama']?>/ <?php echo $data['kwn']?></td>
</tr>
<tr>
    <td style="width: 8%;"></td>
    <td style="width: 30%;">5. Pekerjaan</td>
    <td style="width: 1%;">:</td>
    <td style="width: 61%;"><?php echo $data['pekerjaan']?></td>
</tr>
<tr>
    <td style="width: 8%;"></td>
    <td style="width: 30%;" valign="top">6. Alamat Asal</td>
    <td style="width: 1%;" valign="top">:</td>
    <td style="width: 61%;">Dusun <?php echo $data['dusun']?> RT/RW <?php echo $data['rt']?> Desa <?php echo $data['kel']?> Kecamatan <?php echo $data['kec']?> Kabupaten Lombok Timur</td>
</tr>

<tr>
    <td style="width: 8%;"></td>
    <td style="width: 30%;">7. NIK</td>
    <td style="width: 1%;">:</td>
    <td style="width: 64%;"><?php echo $data['nik']?></td>
</tr>
<tr>
    <td style="width: 8%;"></td>
    <td style="width: 30%;">8. Alamat Tujuan</td>
    <td style="width: 1%;">:</td>
    <td style="width: 61%;"><?php echo $data['tujuan']?></td>
</tr>
<tr>
    <td style="width: 8%;"></td>
    <td style="width: 30%;">9. Tanggal Berangkat</td>
    <td style="width: 1%;">:</td>
    <td style="width: 61%;"><?php echo date('d-m-Y', strtotime($data['tglberangkat']))?></td>
</tr>
<tr>
    <td style="width: 8%;"></td>
    <td style="width: 30%;">10. Transportasi</td>
    <td style="width: 1%;">:</td>
    <td style="width: 61%;"><?php echo $data['transportasi']?></td>
</tr>
<tr>
    <td style="width: 8%;"></td>
    <td style="width: 30%;">11. Lamanya</td>
    <td style="width: 1%;">:</td>
    <td style="width: 61%;"><?php echo $data['lamanya']?></td>
</tr>
<tr>
    <td style="width: 8%;"></td>
    <td style="width: 30%;">12. Keterangan Lain-lain</td>
    <td style="width: 1%;">:</td>
    <td style="width: 61%;"><?php echo $data['ket']?></td>
</tr>

</table>
<br>
<span style="text-align:justify">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Berdasarkan keterangan yang tersebut namanya diatas memang benar penduduk 
desa <?php echo $data['kel']?>. Berkelakuan baik 
tidak pernah tersangkut dalam urusan Kepolisian dan Tindak Pidana lainnya.
</span>
<br>
<span style="text-align:justify">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Demikian surat Keterangan ini dibuat dengan sebenarnya untuk dapat dipergunakan sebagaimana mestinya.</span>
<br><br><br><br><br><br>
<table style="width: 100%" align="right">
<tr>
    <td style="width: 30%; text-align: center">Batuyang, <?php echo date('d F Y');?> 
    <br>Kepala  Desa  Batuyang	
    <br>
    <br>
    <br>
    <br>
    <br>
    <b><u>Drs. SYARAFUDDIN</u></b></td>
</tr>
</table>
</page>