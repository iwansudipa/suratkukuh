<page backtop="10mm" backbottom="10mm" backleft="20mm" backright="20mm">
<table style="width: 100%">
<tr>
    <td style="width: 10%"><img src="../../../img/kab-lotim.png" alt="" width="80"></td>
    <td style="text-align: center; width:80%"><b><span style="font-size:15pt">PEMERINTAH KABUPATEN LOMBOK TIMUR KECAMATAN PRINGGABAYA</span><br>
    <span style="font-size:20pt">DESA BATUYANG</span><br><br><span style="font-size:10pt">Jln Raya Jurusan Mataram-Batuyang kode Pos 83654</span> 
    </b></td>
</tr>
</table>
<hr border="2">
<p style="text-align: center; font-size:14pt;"><b><u>SURAT KETERANGAN NIKAH</u></b><br>
<span style="font-size:12pt">Nomor : <?php echo $data['no_suratkeluar'];?></span></p>
<span style="text-align:justify;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Yang bertanda tangan di bawah ini Kepala Desa Batuyang Kecamatan Pringgabaya Kabupaten Lombok Timur, 
menerangkan dengan sebenarnya Kepada :</span><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1. <b>SUAMI</b>
<table style="width:100%" align="center">
<tr>
    <td style="width: 10%;"></td>
    <td style="width: 30%;">1. Nama</td>
    <td style="width: 1%;">:</td>
    <td style="width: 59%;" colspan="2"><?php echo $data['namas'];?></td>
</tr>
<tr>
    <td style="width: 10%;"></td>
    <td style="width: 30%;">2. Tempat/Tgl. Lahir</td>
    <td style="width: 1%;">:</td>
    <td style="width: 59%;" colspan="2"><?php echo $data['tempatlahirs']?>, <?php echo date('d-m-Y', strtotime($data['tgllahirs']))?></td>
</tr>
<tr>
    <td style="width: 10%;"></td>
    <td style="width: 30%;">3. Jenis Kelamin</td>
    <td style="width: 1%;">:</td>
    <td style="width: 59%;" colspan="2"><?php echo $data['jks']?></td>

</tr>
<tr>
    <td style="width: 10%;"></td>
    <td style="width: 30%;">4. Agama</td>
    <td style="width: 1%;">:</td>
    <td style="width: 59%;" colspan="2"><?php echo $data['agamas']?></td>

</tr>
<tr >
    <td style="width: 10%;" ></td>
    <td style="width: 30%;" >5. Pekerjaan</td>
    <td style="width: 1%;" >:</td>
    <td style="width: 20%;" colspan="2"><?php echo $data['pekerjaans']?></td>
</tr>
<tr>
    <td style="width: 10%;"></td>
    <td style="width: 30%;">6. Alamat</td>
    <td style="width: 1%;">:</td>
    <td style="width: 20%;">Dusun</td>
    <td style="width: 39%;">: <?php echo $data['dusuns']?></td>
</tr>
<tr>
    <td style="width: 10%;"></td>
    <td style="width: 30%;"></td>
    <td style="width: 1%;"></td>
    <td style="width: 20%;">Desa</td>
    <td style="width: 39%;">: <?php echo $data['desas']?></td>
</tr>
<tr>
    <td style="width: 10%;"></td>
    <td style="width: 30%;"></td>
    <td style="width: 1%;"></td>
    <td style="width: 20%;">Kecamatan</td>
    <td style="width: 39%;">: <?php echo $data['kecs']?></td>
</tr>
<tr>
    <td style="width: 10%;"></td>
    <td style="width: 30%;"></td>
    <td style="width: 1%;"></td>
    <td style="width: 20%;">Kabupaten</td>
    <td style="width: 39%;">: Lombok Timur</td>
</tr>
</table>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2. <b>ISTRI</b>
<table style="width:100%" align="center">
<tr>
    <td style="width: 10%;"></td>
    <td style="width: 30%;">1. Nama</td>
    <td style="width: 1%;">:</td>
    <td style="width: 59%;" colspan="2"><?php echo $data['namai'];?></td>
</tr>
<tr>
    <td style="width: 10%;"></td>
    <td style="width: 30%;">2. Tempat/Tgl. Lahir</td>
    <td style="width: 1%;">:</td>
    <td style="width: 59%;" colspan="2"><?php echo $data['tempatlahiri']?>, <?php echo date('d-m-Y', strtotime($data['tgllahiri']))?></td>
</tr>
<tr>
    <td style="width: 10%;"></td>
    <td style="width: 30%;">3. Jenis Kelamin</td>
    <td style="width: 1%;">:</td>
    <td style="width: 59%;" colspan="2"><?php echo $data['jki']?></td>

</tr>
<tr>
    <td style="width: 10%;"></td>
    <td style="width: 30%;">4. Agama</td>
    <td style="width: 1%;">:</td>
    <td style="width: 59%;" colspan="2"><?php echo $data['agamai']?></td>

</tr>
<tr >
    <td style="width: 10%;" ></td>
    <td style="width: 30%;" >5. Pekerjaan</td>
    <td style="width: 1%;" >:</td>
    <td style="width: 20%;" colspan="2"><?php echo $data['pekerjaani']?></td>
</tr>
<tr>
    <td style="width: 10%;"></td>
    <td style="width: 30%;">6. Alamat</td>
    <td style="width: 1%;">:</td>
    <td style="width: 20%;">Dusun</td>
    <td style="width: 39%;">: <?php echo $data['dusuni']?></td>
</tr>
<tr>
    <td style="width: 10%;"></td>
    <td style="width: 30%;"></td>
    <td style="width: 1%;"></td>
    <td style="width: 20%;">Desa</td>
    <td style="width: 39%;">: <?php echo $data['desai']?></td>
</tr>
<tr>
    <td style="width: 10%;"></td>
    <td style="width: 30%;"></td>
    <td style="width: 1%;"></td>
    <td style="width: 20%;">Kecamatan</td>
    <td style="width: 39%;">: <?php echo $data['keci']?></td>
</tr>
<tr>
    <td style="width: 10%;"></td>
    <td style="width: 30%;"></td>
    <td style="width: 1%;"></td>
    <td style="width: 20%;">Kabupaten</td>
    <td style="width: 39%;">: Lombok Timur</td>
</tr>
</table>
<br>
<span style="text-align:justify">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Berdasarkan keterangan yang tersebut namanya diatas memang benar telah menikah syah secara Agama 
Islam yang dilakukan dan disaksikan  oleh <?php echo $data['oleh']?> Desa Batuyang pada Tanggal <?php echo date('d F Y', strtotime($data['tglnikah']))?> di Desa Batuyang Kecamatan Pringgabaya Kabupaten Lombok Timur.</span>
<br><br>
<span style="text-align:justify">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Demikian Surat Keterangan ini dibuat dengan sebenarnya untuk dapat dipergunakan sebagaimana mestinya.</span>
<br><br><br><br><br><br>
<table style="width: 100%" align="right">
<tr>
    <td style="width: 30%; text-align: center">Batuyang, <?php echo date('d F Y');?> 
    <br>Kepala  Desa  Batuyang	
    <br>
    <br>
    <br>
    <br>
    <br>
    <b><u>Drs. SYARAFUDDIN</u></b></td>
</tr>
</table>
</page>