<?php
require_once '../../../assets/html2pdf/vendor/autoload.php';
use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;

try {
    include '../../../config/koneksi.php';
    $sql=$koneksi->query("select * from suratkeluar join nikah on suratkeluar.id_suratkeluar = nikah.id_suratkeluar 
    where id_nikah=$_GET[id] ") or die(mysqli_error($koneksi));
    $data = $sql->fetch_assoc();
    ob_start();
    include '../../suratketerangan/nikah/res/nikah.php';
    $content = ob_get_clean();

    $html2pdf = new Html2Pdf('P', 'A4', 'en');
    $html2pdf->setDefaultFont('arial');
    $html2pdf->writeHTML($content);
    $html2pdf->output('Surat keterangan nikah.pdf');
} catch (Html2PdfException $e) {
    $html2pdf->clean();

    $formatter = new ExceptionFormatter($e);
    echo $formatter->getHtmlMessage();
}