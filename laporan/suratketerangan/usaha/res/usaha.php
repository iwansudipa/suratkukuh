<page backtop="10mm" backbottom="10mm" backleft="20mm" backright="20mm">
<table style="width: 100%">
<tr>
    <td style="width: 10%"><img src="../../../img/kab-lotim.png" alt="" width="80"></td>
    <td style="text-align: center; width:80%"><b><span style="font-size:15pt">PEMERINTAH KABUPATEN LOMBOK TIMUR KECAMATAN PRINGGABAYA</span><br>
    <span style="font-size:20pt">DESA BATUYANG</span><br><span style="font-size:10pt">Jln Raya Jurusan Mataram-Batuyang kode Pos 83654</span> 
    </b></td>
</tr>
</table>
<hr border="2">
<p style="text-align: center; font-size:14pt;"><b><u>SURAT KETERANGAN USAHA</u></b><br>
<span style="font-size:12pt">Nomor : <?php echo $data['no_suratkeluar'];?></span></p>

<table style="width: 100%" >
<tr>
    <td style="width: 100%;" colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Yang bertanda tangan di bawah ini Kepala Desa Batuyang Kecamatan Pringgabaya Kabupaten Lombok Timur, menerangkan dengan sebenarnya Kepada :</td>
</tr>
<tr>
    <td style="width: 33%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1. Nama</td>
    <td style="width: 1%">:</td>
    <td style="width: 66%"><?php echo $data['nama']?></td>
</tr>
<tr>
    <td style="width: 33%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2. Tempat/Tgl.Lahir</td>
    <td style="width: 1%">:</td>
    <td style="width: 66%"><?php echo $data['tempatlahir']?>, <?php echo date('d-m-Y', strtotime($data['tgllahir']))?></td>
</tr>
<tr>
    <td style="width: 33%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3. Jenis Kelamin</td>
    <td style="width: 1%">:</td>
    <td style="width: 66%"><?php echo $data['jk']?></td>
</tr>
<tr>
    <td style="width: 33%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 4. Agama</td>
    <td style="width: 1%">:</td>
    <td style="width: 66%"><?php echo $data['agama']?></td>
</tr>
<tr>
    <td style="width: 33%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 5. Pekerjaan</td>
    <td style="width: 1%">:</td>
    <td style="width: 66%"><?php echo $data['pekerjaan']?></td>
</tr>
<tr>
    <td style="width: 33%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 6. NIK</td>
    <td style="width: 1%">:</td>
    <td style="width: 66%"><?php echo $data['nik']?></td>
</tr>
<tr>
    <td style="width: 33%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 7. Alamat</td>
    <td style="width: 1%" valign="top">:</td>
    <td style="width: 66%">Dusun <?php echo $data['dusun']?> RT/RW <?php echo $data['rt']?> Desa <?php echo $data['kel']?> Kecamatan <?php echo $data['kecamatan']?>
    Kabupaten Lombok Timur</td>
</tr>

<tr>
    <td style="width: 100%; " colspan="3" >
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Berdasarkan keterangan yang tersebut namanya diatas memang benar memiliki usaha yang bergerak dalam bidang Usaha
    <b><?php echo $data['ket']?></b> yang berlokasi di dusun Rumbuk  Desa Batuyang Kecamatan Pringgabaya Kabupaten Lombok Timur. 
    Mulai berusaha sejak tahun <?php echo $data['tahunusaha']?> sampai saat ini.
    </td>
</tr>
<tr>
    <td style="width: 100%;" colspan="3" >
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Surat keterangan ini diberikan kepada yang bersangkutan sebagai persyaratan Pinjaman Tambah Modal Usaha di Bank <?php echo $data['pinjamanbank']?>.
    </td>
</tr>
<tr>
    <td style="width: 100%; height:1%;" colspan="3" >
    </td>
</tr>
<tr>
    <td style="width: 100%;" colspan="3" >
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Demikian Surat Keterangan ini dibuat dengan sebenarnya untuk dapat dipergunakan sebagaimana mestinya.
    </td>
</tr>
</table>
<br><br><br><br><br><br>
<table style="width: 100%" align="right">
<tr>
    <td style="width: 25%; text-align: center">Batuyang, <?php echo date('d F Y');?> 
    <br>Kepala  Desa  Batuyang	
    <br>
    <br>
    <br>
    <br>
    <br>
    <b><u>Drs. SYARAFUDDIN</u></b></td>
</tr>
</table>
</page>