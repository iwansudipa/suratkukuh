<page backtop="10mm" backbottom="10mm" backleft="20mm" backright="20mm">
<style>
.tabel {
  border-collapse: collapse;
  width: 100%;
}

.tabel td {
  border: 1px solid;
  text-align: left;
  padding: 8px;
}
.tabel th {
  border: 1px solid;
  text-align: center;
  background: #FFEEEE;
  padding: 8px;
}

</style>
<h2 style="text-align: center;">LAPORAN SURAT KELUAR</h2>

<table class="tabel" style="width:100%" align="center">
  <tr>
    <th>No</th>
    <th>No. Surat</th>
    <th>Nama</th>
    <th>Tgl. Surat</th>
    <th>Kepada</th>
    <th>Perihal</th>
    <th>Jenis Surat</th>
  </tr>
  <?php 
    $no = 1;
    while($data=$sql->fetch_assoc()){
  ?>
  <tr>
    <td><?php echo $no++;?></td>
    <td ><?php echo $data['no_suratkeluar'];?></td>
    <td ><?php echo $data['nama'];?></td>
    <td><?php echo date('d-m-Y', strtotime($data['tgl_suratkeluar']));?></td>
    <td><?php echo $data['kepada'];?></td>
    <td ><?php echo $data['perihal'];?></td>
    <td ><?php echo $data['jenis_surat'];?></td>
  </tr>
  <?php
    }
  ?>
</table>
</page>