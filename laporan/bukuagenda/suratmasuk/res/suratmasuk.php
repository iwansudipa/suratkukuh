<style>
.tabel {
  border-collapse: collapse;
  width: 100%;
}

.tabel td {
  border: 1px solid;
  text-align: left;
  padding: 8px;
}
.tabel th {
  border: 1px solid;
  text-align: center;
  background: #FFEEEE;
  padding: 8px;
}
</style>
<page backtop="10mm" backbottom="10mm" backleft="20mm" backright="20mm">
<h2 style="text-align: center;">LAPORAN PERIODE SURAT MASUK</h2>
<h3 style="text-align: center;">Dari <?php echo date('d F Y', strtotime($_GET['dari']))?> Sampai <?php echo date('d F Y', strtotime($_GET['sampai']))?></h3>

<table class="tabel" align="center">
  <tr>
    <th>No</th>
    <th>No. Surat</th>
    <th>Tgl. Surat</th>
    <th>Tgl. Terima</th>
    <th>Sifat</th>
    <th>Asal Surat</th>
    <th>Perihal</th>
    <th style="width: 12%">Status Disposisi</th>
  </tr>
  <?php 
    $no = 1;
    while($data=$sql->fetch_assoc()){
  ?>
  <tr>
    <td style="width: 5%"><?php echo $no++;?></td>
    <td style="width: 24%"><?php echo $data['no_suratmasuk'];?></td>
    <td style="width: 2%"><?php echo date('d-m-Y', strtotime($data['tgl_suratmasuk']));?></td>
    <td style="width: 2%"><?php echo date('d-m-Y', strtotime($data['tgl_terima']));?></td>
    <td style="width: 10%"><?php echo $data['sifat'];?></td>
    <td style="width: 10%"><?php echo $data['asal'];?></td>
    <td style="width: 15%"><?php echo $data['perihal'];?></td>
    <td ><?php echo $data['status_disposisi'];?></td>
  </tr>
  <?php
    }
  ?>
</table>
</page>