<?php
require_once '../../../assets/html2pdf/vendor/autoload.php';
use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;

try {
    $koneksi = new mysqli("localhost","root","","db_siades");
    $sql=$koneksi->query("select * from suratmasuk");
    ob_start();
    include '../../bukuagenda/suratmasuk/res/allsuratmasuk.php';
    $content = ob_get_clean();

    $html2pdf = new Html2Pdf('P', 'A4', 'en');
    $html2pdf->setDefaultFont('times');
    $html2pdf->writeHTML($content);
    $html2pdf->output('laporan allsuratmasuk.pdf');
} catch (Html2PdfException $e) {
    $html2pdf->clean();

    $formatter = new ExceptionFormatter($e);
    echo $formatter->getHtmlMessage();
}