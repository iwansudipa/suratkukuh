<?php
require_once '../../assets/html2pdf/vendor/autoload.php';
use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;

try {
    $koneksi = new mysqli("localhost","root","","db_siades");
    $sql=$koneksi->query("select * from suratmasuk join disposisi on suratmasuk.id_suratmasuk=disposisi.id_suratmasuk where id_disposisi=$_GET[id]");
    $data=$sql->fetch_assoc();
    $sql2=$koneksi->query("select * from kepaladesa");
    $data2=$sql2->fetch_assoc();
    ob_start();
    include '../disposisi/res/disposisi.php';
    $content = ob_get_clean();

    $html2pdf = new Html2Pdf('P', 'A6', 'en');
    $html2pdf->setDefaultFont('arial');
    $html2pdf->writeHTML($content);
    $html2pdf->output('disposisi.pdf');
} catch (Html2PdfException $e) {
    $html2pdf->clean();

    $formatter = new ExceptionFormatter($e);
    echo $formatter->getHtmlMessage();
}