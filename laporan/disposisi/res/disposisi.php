<style>
.tabel {
  border-collapse: collapse;
  width: 100%;
}

.tabel td {
  text-align: left;
  padding: 8px;
}
.tabel th {
  text-align: center;
  padding: 8px;
}
</style>
<page backtop="1mm" backbottom="1mm" backleft="3mm" backright="3mm">
<table style="width: 100%">
</table>
<h5 style="text-align:center">LEMBAR PENERUS DISPOSISI</h5>
<table style="width:100%; font-size: 9pt" align="center" class="tabel">
<tr>
    <td style="width:50%; border: 1px solid;">Index / Kode : </td>
    <td style="width:50%; text-align:center; border: 1px solid;">Tanggal Penyelesaian</td>
</tr>
</table>
<table style="width:100%; font-size: 9pt"  align="center">
<tr>
    <td style="width:40%; padding-left: 8px; padding-bottom: 8px;">Perihal</td>
    <td style="width:60%; padding-bottom: 8px;">: <?php echo $data['perihal'];?> </td>
</tr>
<tr>
    <td style="width:40%; padding-left: 8px;">Tanggal / No</td>
    <td style="width:60%">: <?php echo $data['no_suratmasuk'];?> </td>
</tr>
<tr>
    <td style="width:40%; padding-left: 8px; padding-bottom: 8px;">ASAL</td>
    <td style="width:60%; padding-bottom: 8px;">: <?php echo $data['asal'];?> </td>
</tr>
<tr>
    <td style="width:40%; padding-left: 8px;">Tgl. Penerimaan</td>
    <td style="width:60%; ">: <?php echo $data['tgl_terima'];?> </td>
</tr>
</table>
<table style="width:100%; border-collapse: collapse; font-size: 9pt; text-align:center;" align="center">
<tr>
    <td  style="width:50%; height:240; border:1px solid; padding-left: 8px; padding-right: 8px;  " valign="top">Intruksi / Informasi :<p style="text-align:left"><?php echo $data['informasi']?></p></td>
    <td  style="width:50%; height:240; border:1px solid; padding-left: 8px; padding-right: 8px; " valign="top">Diteruskan kepada :<p style="text-align:left"><?php echo $data['diteruskan']?></p></td>
</tr>
</table>
<table style="width: 100%; padding-left: 8px; font-size:6pt " align="center">
<tr>
    <td style="width: 100%;">1. Kepada bawahan "intruksi" dan atau "informasi"</td>
</tr>
<tr>
    <td style="width: 100%;">2. Kepada atasan "informasi coret intruksi"</td>
</tr>
</table>
<table style="width: 100%; font-size:6pt"  align="right">
<tr>
    <td style="width: 30%; text-align: center">Batuyang, <?php echo date('d F Y');?> 
    <!-- <br>Kepala  Desa  Batuyang	 -->
    <br><img src="../../img/<?php echo $data2['ttd']?>" alt="" width="80" height="30">
    <br>
    <b><u><?php echo $data2['nama'];?></u></b></td>
</tr>
</table>
</page>