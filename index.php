<?php
  session_start();
  error_reporting(0);
  $page=$_GET['page'];
  $aksi=$_GET['aksi'];
  include 'config/koneksi.php';

  if (!isset($_SESSION['admin'])){
    header('location:config/login.php');
    exit();
  }else {
    $admin = $_SESSION['admin'];
  }

  $sql=$koneksi->query("select * from user where id_user='$admin'");
  $data = $sql->fetch_assoc();
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Siades</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" href="assets/sweet/sweetalert.css">
  <link rel="stylesheet" href="assets/croppie/croppie.css">
  <!-- jQuery 3 -->
  <script src="bower_components/jquery/dist/jquery.min.js"></script>
  <script src="assets/sweet/sweetalert.js"></script>
  <script src="assets/croppie/croppie.js"></script>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-green sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="index.php" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>AS</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Administrasi SURAT</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
<?php 
    if ($data['foto']=="avatar5.png" or $data['foto']=="avatar3.png") {
?>
              <img src="img/user/<?php echo $data['foto']?>" class="user-image" alt="User Image">
<?php
  }else {
?>
              <img src="img/<?php echo $data['foto']?>" class="user-image" alt="User Image">
<?php
  }
?>
              
              <span class="hidden-xs"><?php echo $data['nama']?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
<?php 
    if ($data['foto']=="avatar5.png" or $data['foto']=="avatar3.png") {
?>
              <img src="img/user/<?php echo $data['foto']?>" class="img-circle" alt="User Image">
<?php
  }else {
?>
              <img src="img/<?php echo $data['foto']?>" class="img-circle" alt="User Image">
<?php
  }
?>
                <p>
                  <?php echo $data['nama']?> - <?php echo $data['level']?>
                  <small><?php echo $data['email']?></small>
                </p>
              </li>
              <!-- Menu Body -->
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="?page=profile" class="btn btn-block btn-sm btn-social btn-instagram btn-flat"><i class="fa fa-user"></i> Profile</a>
                </div>
                <div class="pull-right">
                  <a href="?page=logout" class="btn btn-block btn-sm btn-social btn-google btn-flat"><i class="fa fa-power-off"></i> Logout</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
      <div class="pull-left image" >
<?php 
    if ($data['foto']=="avatar5.png" or $data['foto']=="avatar3.png") {
?>
              <img src="img/user/<?php echo $data['foto']?>" class="img-responsive img-circle" alt="User Image">
<?php
  }else {
?>
              <img src="img/<?php echo $data['foto']?>" class="img-responsive img-circle" alt="User Image">
<?php
  }
?>
         
        </div>
        <div class="pull-left info">
          <p><?php echo $data['nama']?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">Navigasi Menu</li>
        <li class="<?php if ($page=="") {
          echo "active";
        }?>">
          <a href="index.php">
            <i class="fa fa-th"></i> <span>Dashboard</span>
          </a>
        </li>
        <li class="<?php if ($page=="penduduk") {
          echo "active";
        }?>">
          <a href="?page=penduduk">
            <i class="fa fa-users"></i> <span>Penduduk</span>
          </a>
        </li>
        <li class="<?php if ($page=="suratmasuk") {
          echo "active";
        }?>">
          <a href="?page=suratmasuk">
            <i class="fa fa-envelope"></i> <span>Surat Masuk</span>
          </a>
        </li>
        <li class="<?php if ($page=="suratkeluar") {
          echo "active";
        }?>" >
          <a href="?page=suratkeluar">
            <i class="fa fa-envelope-o"></i> <span>Surat Keluar</span>
          </a>
        </li>
        <li class="header">Laporan</li>
        <li class="treeview <?php if ($page=="bukusuratmasuk" or $page=="bukusuratkeluar" ) {
          echo "active";
        }?>">
          <a href="#">
            <i class="fa fa-archive"></i>
            <span>Buku Agenda</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?php if ($page=="bukusuratmasuk") {
              echo "active";
            }?>"><a href="?page=bukusuratmasuk"><i class="fa fa-circle-o text-red"></i> Surat Masuk</a></li>
            <li class="<?php if ($page=="bukusuratkeluar") {
              echo "active";
            }?>"><a href="?page=bukusuratkeluar"><i class="fa fa-circle-o text-yellow"></i> Surat Keluar</a></li>
          </ul>
        </li>
        <li class="<?php if ($page=="disposisi") {
          echo "active";
        }?>">
          <a href="?page=disposisi">
            <i class="fa fa-book"></i> <span>Disposisi</span>
          </a>
        </li>
        <li class="treeview <?php if ($page=="usaha" || $page=="nikah" || $page=="belumnikah" || $page=="kepolisian"
        || $page=="bepergian" || $page=="kehilangan" || $page=="domisili" || $page=="meninggal") {
          echo "active";
        }?>">
          <a href="#">
            <i class="fa fa-file-pdf-o"></i>
            <span>Surat Keterangan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?php if ($page=="usaha") {
              echo "active";
            }?>"><a href="?page=usaha"><i class="fa fa-circle-o text-black"></i> Usaha</a></li>
            <li class="<?php if ($page=="nikah") {
              echo "active";
            }?>"><a href="?page=nikah"><i class="fa fa-circle-o text-aqua"></i> Nikah</a></li>
            <li class="<?php if ($page=="belumnikah") {
              echo "active";
            }?>"><a href="?page=belumnikah"><i class="fa fa-circle-o text-blue"></i> Belum Nikah</a></li>
            <li class="<?php if ($page=="kepolisian") {
              echo "active";
            }?>"><a href="?page=kepolisian"><i class="fa fa-circle-o text-danger"></i> Catatan Kepolisian</a></li>
            <li class="<?php if ($page=="bepergian") {
              echo "active";
            }?>"><a href="?page=bepergian"><i class="fa fa-circle-o text-gray"></i> Bepergian</a></li>
            <li class="<?php if ($page=="kehilangan") {
              echo "active";
            }?>"><a href="?page=kehilangan"><i class="fa fa-circle-o text-green"></i> Kehilangan</a></li>
            <li class="<?php if ($page=="domisili") {
              echo "active";
            }?>"><a href="?page=domisili"><i class="fa fa-circle-o text-info"></i> Domisili</a></li>
            <li class="<?php if ($page=="meninggal") {
              echo "active";
            }?>"><a href="?page=meninggal"><i class="fa fa-circle-o text-light-blue"></i> Meninggal Dunia</a></li>
          </ul>
        </li>
        <li class="header">Pengaturan</li>
        <li class="<?php if ($page=="pengguna") {
          echo "active";
        }?>">
          <a href="?page=pengguna">
            <i class="fa fa-user"></i> <span>Pengguna</span>
          </a>
        </li>
        <li class="<?php if ($page=="kepaladesa") {
          echo "active";
        }?>">
          <a href="?page=kepaladesa">
            <i class="fa fa-black-tie"></i> <span>Kepala Desa</span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
<?php
  if ($page=="suratmasuk") {
    if ($aksi=="") {
      include 'page/suratmasuk/suratmasuk.php';
    }elseif ($aksi=="tambah") {
      include 'page/suratmasuk/tambah.php';
    }elseif ($aksi=="hapus") {
      include 'page/suratmasuk/hapus.php';
    }elseif ($aksi=="edit") {
      include 'page/suratmasuk/edit.php';
    }elseif ($aksi=="detail") {
      include 'page/suratmasuk/detail.php';
    }
  }elseif ($page=="suratkeluar") {
    if ($aksi=="") {
      include 'page/suratkeluar/suratkeluar.php';
    }elseif ($aksi=="edit") {
      include 'page/suratkeluar/edit.php';
    }elseif ($aksi=="tambah") {
      include 'page/suratkeluar/tambah.php';
    }elseif ($aksi=="hapus") {
      include 'page/suratkeluar/hapus.php';
    }
  }elseif ($page=="bukusuratmasuk") {
    if ($aksi=="") {
      include 'page/bukuagenda/suratmasuk/bukusuratmasuk.php';
    }
  }elseif ($page=="bukusuratkeluar") {
    if ($aksi=="") {
      include 'page/bukuagenda/suratkeluar/bukusuratkeluar.php';
    }
  }elseif ($page=="disposisi") {
    if ($aksi=="") {
      include 'page/disposisi/disposisi.php';
    }
  }elseif ($page=="usaha") {
    if ($aksi=="") {
      include 'page/surat/usaha/usaha.php';
    }elseif ($aksi=="tambah") {
      include 'page/surat/usaha/tambah.php';
    }elseif ($aksi=="edit") {
      include 'page/surat/usaha/edit.php';
    }elseif ($aksi=="hapus") {
      include 'page/surat/usaha/hapus.php';
    }
  }elseif ($page=="nikah") {
    if ($aksi=="") {
      include 'page/surat/nikah/nikah.php';
    }elseif ($aksi=="tambah") {
      include 'page/surat/nikah/tambah.php';
    }elseif ($aksi=="edit") {
      include 'page/surat/nikah/edit.php';
    }elseif ($aksi=="hapus") {
      include 'page/surat/nikah/hapus.php';
    }
  }elseif ($page=="belumnikah") {
    if ($aksi=="") {
      include 'page/surat/belumnikah/belumnikah.php';
    }elseif ($aksi=="tambah") {
      include 'page/surat/belumnikah/tambah.php';
    }elseif ($aksi=="edit") {
      include 'page/surat/belumnikah/edit.php';
    }elseif ($aksi=="hapus") {
      include 'page/surat/belumnikah/hapus.php';
    }
  }elseif ($page=="kepolisian") {
    if ($aksi=="") {
      include 'page/surat/kepolisian/kepolisian.php';
    }elseif ($aksi=="tambah") {
      include 'page/surat/kepolisian/tambah.php';
    }elseif ($aksi=="edit") {
      include 'page/surat/kepolisian/edit.php';
    }elseif ($aksi=="hapus") {
      include 'page/surat/kepolisian/hapus.php';
    }
  }elseif ($page=="bepergian") {
    if ($aksi=="") {
      include 'page/surat/bepergian/bepergian.php';
    }elseif ($aksi=="tambah") {
      include 'page/surat/bepergian/tambah.php';
    }elseif ($aksi=="edit") {
      include 'page/surat/bepergian/edit.php';
    }elseif ($aksi=="hapus") {
      include 'page/surat/bepergian/hapus.php';
    }
  }elseif ($page=="kehilangan") {
    if ($aksi=="") {
      include 'page/surat/kehilangan/kehilangan.php';
    }elseif ($aksi=="tambah") {
      include 'page/surat/kehilangan/tambah.php';
    }elseif ($aksi=="edit") {
      include 'page/surat/kehilangan/edit.php';
    }elseif ($aksi=="hapus") {
      include 'page/surat/kehilangan/hapus.php';
    }
  }elseif ($page=="domisili") {
    if ($aksi=="") {
      include 'page/surat/domisili/domisili.php';
    }elseif ($aksi=="tambah") {
      include 'page/surat/domisili/tambah.php';
    }elseif ($aksi=="edit") {
      include 'page/surat/domisili/edit.php';
    }elseif ($aksi=="hapus") {
      include 'page/surat/domisili/hapus.php';
    }
  }elseif ($page=="meninggal") {
    if ($aksi=="") {
      include 'page/surat/meninggal/meninggal.php';
    }elseif ($aksi=="tambah") {
      include 'page/surat/meninggal/tambah.php';
    }elseif ($aksi=="edit") {
      include 'page/surat/meninggal/edit.php';
    }elseif ($aksi=="hapus") {
      include 'page/surat/meninggal/hapus.php';
    }
  }elseif ($page=="pengguna") {
    if ($aksi=="") {
      include 'page/pengguna/pengguna.php';
    }elseif ($aksi=="tambah") {
      include 'page/pengguna/tambah.php';
    }elseif ($aksi=="edit") {
      include 'page/pengguna/edit.php';
    }elseif ($aksi=="hapus") {
      include 'page/pengguna/hapus.php';
    }
  }elseif ($page=="penduduk") {
      if ($aksi=="") {
        include 'page/penduduk/penduduk.php';
      }elseif ($aksi=="edit") {
        include 'page/penduduk/edit.php';
      }elseif ($aksi=="hapus") {
        include 'page/penduduk/hapus.php';
      }
  }elseif ($page=="kepaladesa") {
    if ($aksi==""){
      include 'page/kepaladesa/kepaladesa.php';
    }elseif ($aksi=="edit") {
      include 'page/kepaladesa/edit.php';
    }
  }elseif ($page=="logout") {
    if ($aksi==""){
      include 'page/logout.php';
    }
  }elseif ($page=="profile") {
      if ($aksi==""){
        include 'page/profile.php';
      }
  }else {
    include 'page/home.php';
  }
?>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; <?php echo date("Y"); ?> <a>Desa Kukuh</a>.</strong> All rights
    reserved.
  </footer>
  <!-- Control Sidebar -->
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery UI 1.11.4 -->
<script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- InputMask -->
<script src="plugins/input-mask/jquery.inputmask.js"></script>
<script src="plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- Morris.js charts -->
<script src="bower_components/raphael/raphael.min.js"></script>
<script src="bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- DataTables -->
<script src="bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- Select2 -->
<script src="bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
    //Date picker
    $('#datepicker').datepicker({
      format: 'dd-mm-yyyy',
      todayHighlight: true,
      autoclose: true
    })
    $('#datepicker2').datepicker({
      format: 'dd-mm-yyyy',
      todayHighlight: true,
      autoclose: true
    })
    $('#datepicker3').datepicker({
      format: 'dd-mm-yyyy',
      todayHighlight: true,
      autoclose: true
    })
    //Initialize Select2 Elements
    $('.select2').select2()
  })
</script>
</body>
</html>