<?php
  if (isset($_POST['cari'])) {
    $vdari=$_POST['dari'];
    $vsampai=$_POST['sampai'];
  }
?> 
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      Surat Masuk
        <small>Buku Agenda</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Buku Agenda</li>
      </ol>
    </section>
    <!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Laporan Surat Masuk</h3>
            </div>
            
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="" method="POST">
              <div class="box-body">
                <div class="form-group">
                  <label class="col-sm-2 control-label">Dari Tanggal:</label>

                  <div class="col-sm-10">
                  <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" name="dari" class="form-control pull-right" id="datepicker" placeholder="Masukkan Tanggal" 
                  value="<?php if (isset($_POST['cari'])) {
                    echo $vdari;
                  }?>" required>
                  </div>
                <!-- /.input group -->
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Sampai Tanggal:</label>

                  <div class="col-sm-10">
                  <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" name="sampai" class="form-control pull-right" id="datepicker2" placeholder="Masukkan Tanggal" 
                  value="<?php if (isset($_POST['cari'])) {
                    echo $vsampai;
                  }?>" required>
                  </div>
                <!-- /.input group -->
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a href="javascript:history.go(-1)" class="btn btn-default"><i class="fa fa-arrow-left"></i> Kembali</a>
                <a href="../laporan/bukuagenda/suratmasuk/allsuratmasuk.php" target="_blank" class="btn btn-primary pull-right"><i class="fa fa-print"></i> Cetak Semua</a>
                <button type="submit" name="cari" class="btn btn-info pull-right" style="margin-right: 5px"><i class="fa fa-eye "></i> Lihat</button>
             
<?php 
  if (isset($_POST['cari'])) {
    $dari=date('Y-m-d',strtotime($_POST['dari']));
    $sampai=date('Y-m-d',strtotime($_POST['sampai']));
    $sql=$koneksi->query("select * from suratmasuk where tgl_suratmasuk between '$dari' and '$sampai' ") or die(mysqli_error($koneksi));
    $cek=$sql->num_rows;
    if ($cek==0) {
?>
<?php 
}else 
{
?>
    <a href="../laporan/bukuagenda/suratmasuk/suratmasuk.php?dari=<?php echo $dari;?>&sampai=<?php echo $sampai;?>" target="_blank" class="btn btn-success pull-right" style="margin-right: 5px"><i class="fa fa-print"></i> Cetak Periode</a>
<?php }
?>
<?php }
?>

              </div>
              <!-- /.box-footer -->
            </form>
          </div>
  <?php 
  if (isset($_POST['cari'])) {
    $dari=date('Y-m-d',strtotime($_POST['dari']));
    $sampai=date('Y-m-d',strtotime($_POST['sampai']));
    
    $sql=$koneksi->query("select * from suratmasuk where tgl_suratmasuk between '$dari' and '$sampai' ") or die(mysqli_error($koneksi));
    $cek=$sql->num_rows;
    if ($cek==0) {
?>
 <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-ban"></i> Keterangan!</h4>
        Data surat masuk dari tanggal <?php echo date('d F Y', strtotime($dari))?> sampai <?php echo date('d F Y', strtotime($sampai))?> KOSONG!!!.
      </div>
<?php 
  }else {

?>
          <!-- /.box -->
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">Data surat masuk dari tanggal <?php echo date('d F Y', strtotime($dari))?> sampai <?php echo date('d F Y', strtotime($sampai))?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped table-responsive">
                <thead>
                <tr>
                  <th>No</th>
                  <th>No Surat</th>
                  <th>Tgl. Surat</th>
                  <th>Tgl. Terima</th>
                  <th>Asal</th>
                  <th>Sifat</th>
                  <th>Perihal</th>
                  <th>Status Disposisi</th>
                </tr>
                </thead>
                <tbody>
<?php
  $no = 1;
  while ($data=$sql->fetch_assoc()){
?>
                <tr>
                  <td><?php echo $no++;?></td>
                  <td><?php echo $data['no_suratmasuk'];?></td>
                  <td><?php echo date('d-m-Y', strtotime($data['tgl_suratmasuk']));?></td>
                  <td><?php echo date('d-m-Y', strtotime($data['tgl_terima']));?></td>
                  <td><?php echo $data['asal'];?></td>
                  <td><?php echo $data['sifat'];?></td>
                  <td><?php echo $data['perihal'];?></td>
                  <td><i class="<?php if ($data['status_disposisi']=='Sudah') {
                    echo"fa fa-check btn-success btn-xs";
                  }else {
                    echo"fa fa-close btn-danger btn-xs";
                  }?>"></i> 
                  <?php echo $data['status_disposisi'];?></td>
                </tr>
<?php
  }
?>
                </tbody>
                <tfoot>
                <tr>
                  <th>No</th>
                  <th>No Surat</th>
                  <th>Tgl. Surat</th>
                  <th>Tgl. Terima</th>
                  <th>Asal</th>
                  <th>Sifat</th>
                  <th>Perihal</th>
                  <th>Status Disposisi</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
<?php
  }
?>
<?php
  }else{
?>
      <div class="alert alert-warning alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-warning"></i> Perhatian!</h4>
        Silahkan isi dari tanggal sampai tanggal berapa data surat masuk yang ingin di cetak perperiode
      </div>
<?php 
  }
?>
      </div>
</section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->