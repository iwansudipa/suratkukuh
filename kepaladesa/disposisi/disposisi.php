  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Disposisi
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Disposisi</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
    <div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">Data Disposisi Surat Masuk</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>No Surat</th>
                  <th width="70px">Tgl. Surat</th>
                  <th width="80px">Tgl. Terima</th>
                  <th>Asal</th>
                  <th>Sifat</th>
                  <th>Perihal</th>
                  <th>Diteruskan</th>
                  <th>Informasi</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
<?php
  $no = 1;
  $sql = $koneksi->query("select * from suratmasuk join disposisi on suratmasuk.id_suratmasuk=disposisi.id_suratmasuk");
  while ($data=$sql->fetch_assoc()){
?>
                <tr>
                  <td><?php echo $no++;?></td>
                  <td><?php echo $data['no_suratmasuk'];?></td>
                  <td><?php echo date('d-m-Y', strtotime($data['tgl_suratmasuk']));?></td>
                  <td><?php echo date('d-m-Y', strtotime($data['tgl_terima']))?></td>
                  <td><?php echo $data['asal'];?></td>
                  <td><?php echo $data['sifat'];?></td>
                  <td><?php echo $data['perihal'];?></td>
                  <td><?php echo $data['diteruskan'];?></td>
                  <td><?php echo $data['informasi'];?></td>
                  <td class="text-center">
                    <a href="../laporan/disposisi/disposisi.php?id=<?php echo $data['id_disposisi']?>" target="_blank" data-toggle="tooltip" data-placement="top" title="Cetak" class="btn btn-success btn-xs" ><i class="fa fa-print"></i></a>
                  </td>
                </tr>
<?php
  }
?>
                </tbody>
                <tfoot>
                <tr>
                  <th>No</th>
                  <th>No Surat</th>
                  <th>Tgl. Surat</th>
                  <th>Tgl. Terima</th>
                  <th>Asal</th>
                  <th>Sifat</th>
                  <th>Perihal</th>
                  <th>Diteruskan</th>
                  <th>Informasi</th>
                  <th>Aksi</th>
                </tr>
                </tfoot>
              </table>
              <a href="javascript:history.go(-1)" class="btn btn-default"><i class="fa fa-arrow-left"></i> Kembali</a>
            </div>
            <!-- /.box-body -->
          </div>
    </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->