/*
SQLyog Ultimate v12.4.3 (64 bit)
MySQL - 10.1.37-MariaDB : Database - db_siades
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_siades` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `db_siades`;

/*Table structure for table `belumnikah` */

DROP TABLE IF EXISTS `belumnikah`;

CREATE TABLE `belumnikah` (
  `id_belumnikah` int(11) NOT NULL AUTO_INCREMENT,
  `id_suratkeluar` int(11) DEFAULT NULL,
  `nik` varchar(50) DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `tempatlahir` varchar(50) DEFAULT NULL,
  `tgllahir` date DEFAULT NULL,
  `jk` varchar(50) DEFAULT NULL,
  `agama` varchar(50) DEFAULT NULL,
  `pekerjaan` varchar(50) DEFAULT NULL,
  `dusun` varchar(50) DEFAULT NULL,
  `rt` varchar(50) DEFAULT NULL,
  `kel` varchar(50) DEFAULT NULL,
  `kecamatan` varchar(50) DEFAULT NULL,
  `tujuan` varchar(50) DEFAULT NULL,
  `alias` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_belumnikah`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `belumnikah` */

/*Table structure for table `bepergian` */

DROP TABLE IF EXISTS `bepergian`;

CREATE TABLE `bepergian` (
  `id_bepergian` int(11) NOT NULL AUTO_INCREMENT,
  `id_suratkeluar` int(11) DEFAULT NULL,
  `nik` varchar(50) DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `tempatlahir` varchar(50) DEFAULT NULL,
  `tgllahir` date DEFAULT NULL,
  `jk` varchar(50) DEFAULT NULL,
  `agama` varchar(50) DEFAULT NULL,
  `kwn` varchar(50) DEFAULT NULL,
  `pekerjaan` varchar(50) DEFAULT NULL,
  `dusun` varchar(50) DEFAULT NULL,
  `rt` varchar(50) DEFAULT NULL,
  `kel` varchar(50) DEFAULT NULL,
  `kec` varchar(50) DEFAULT NULL,
  `tujuan` varchar(50) DEFAULT NULL,
  `tglberangkat` date DEFAULT NULL,
  `transportasi` enum('Pesawat','Kapal Laut') DEFAULT NULL,
  `lamanya` varchar(50) DEFAULT NULL,
  `ket` text,
  PRIMARY KEY (`id_bepergian`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `bepergian` */

/*Table structure for table `disposisi` */

DROP TABLE IF EXISTS `disposisi`;

CREATE TABLE `disposisi` (
  `id_disposisi` int(11) NOT NULL AUTO_INCREMENT,
  `id_suratmasuk` int(11) DEFAULT NULL,
  `diteruskan` varchar(50) DEFAULT NULL,
  `informasi` text,
  PRIMARY KEY (`id_disposisi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `disposisi` */

/*Table structure for table `domisili` */

DROP TABLE IF EXISTS `domisili`;

CREATE TABLE `domisili` (
  `id_domisili` int(11) NOT NULL AUTO_INCREMENT,
  `id_suratkeluar` int(11) DEFAULT NULL,
  `nik` varchar(50) DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `tempatlahir` varchar(50) DEFAULT NULL,
  `tgllahir` date DEFAULT NULL,
  `jk` varchar(50) DEFAULT NULL,
  `agama` varchar(50) DEFAULT NULL,
  `pekerjaan` varchar(50) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `dusun` varchar(50) DEFAULT NULL,
  `rt` varchar(50) DEFAULT NULL,
  `desa` varchar(50) DEFAULT NULL,
  `kec` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_domisili`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `domisili` */

/*Table structure for table `jenissurat` */

DROP TABLE IF EXISTS `jenissurat`;

CREATE TABLE `jenissurat` (
  `id_jenissurat` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_surat` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_jenissurat`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `jenissurat` */

insert  into `jenissurat`(`id_jenissurat`,`jenis_surat`) values 
(1,'Usaha'),
(2,'Nikah'),
(3,'Belum Nikah'),
(4,'Catatan Kepolisian'),
(5,'Bepergian'),
(6,'Kehilangan'),
(7,'Domisili'),
(8,'Meninggal Dunia');

/*Table structure for table `kehilangan` */

DROP TABLE IF EXISTS `kehilangan`;

CREATE TABLE `kehilangan` (
  `id_kehilangan` int(11) NOT NULL AUTO_INCREMENT,
  `id_suratkeluar` int(11) DEFAULT NULL,
  `nik` varchar(50) DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `tempatlahir` varchar(50) DEFAULT NULL,
  `tgllahir` date DEFAULT NULL,
  `jk` varchar(50) DEFAULT NULL,
  `agama` varchar(50) DEFAULT NULL,
  `pekerjaan` varchar(50) DEFAULT NULL,
  `dusun` varchar(50) DEFAULT NULL,
  `rt` varchar(50) DEFAULT NULL,
  `kel` varchar(50) DEFAULT NULL,
  `kec` varchar(50) DEFAULT NULL,
  `kehilangan` varchar(50) DEFAULT NULL,
  `ket` text,
  PRIMARY KEY (`id_kehilangan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `kehilangan` */

/*Table structure for table `kepaladesa` */

DROP TABLE IF EXISTS `kepaladesa`;

CREATE TABLE `kepaladesa` (
  `id_kepaladesa` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) DEFAULT NULL,
  `ttd` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_kepaladesa`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `kepaladesa` */

insert  into `kepaladesa`(`id_kepaladesa`,`nama`,`ttd`) values 
(1,'Drs, SYARAFUDDIN','ttd-1576239123.png');

/*Table structure for table `kepolisian` */

DROP TABLE IF EXISTS `kepolisian`;

CREATE TABLE `kepolisian` (
  `id_kepolisian` int(11) NOT NULL AUTO_INCREMENT,
  `id_suratkeluar` int(11) DEFAULT NULL,
  `nik` varchar(50) DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `tempatlahir` varchar(50) DEFAULT NULL,
  `tgllahir` date DEFAULT NULL,
  `jk` varchar(50) DEFAULT NULL,
  `agama` varchar(50) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `pekerjaan` varchar(50) DEFAULT NULL,
  `dusun` varchar(50) DEFAULT NULL,
  `rt` varchar(50) DEFAULT NULL,
  `kel` varchar(50) DEFAULT NULL,
  `kec` varchar(50) DEFAULT NULL,
  `keperluan` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_kepolisian`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `kepolisian` */

/*Table structure for table `meninggal` */

DROP TABLE IF EXISTS `meninggal`;

CREATE TABLE `meninggal` (
  `id_meninggal` int(11) NOT NULL AUTO_INCREMENT,
  `id_suratkeluar` int(11) DEFAULT NULL,
  `nik` varchar(50) DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `tempatlahir` varchar(50) DEFAULT NULL,
  `tgllahir` date DEFAULT NULL,
  `jk` varchar(50) DEFAULT NULL,
  `agama` varchar(50) DEFAULT NULL,
  `pekerjaan` varchar(50) DEFAULT NULL,
  `dusun` varchar(50) DEFAULT NULL,
  `rt` varchar(50) DEFAULT NULL,
  `kel` varchar(50) DEFAULT NULL,
  `kec` varchar(50) DEFAULT NULL,
  `tglmeninggal` date DEFAULT NULL,
  `disebabkan` varchar(50) DEFAULT NULL,
  `tempat` text,
  `dikebumikan` text,
  PRIMARY KEY (`id_meninggal`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `meninggal` */

/*Table structure for table `nikah` */

DROP TABLE IF EXISTS `nikah`;

CREATE TABLE `nikah` (
  `id_nikah` int(11) NOT NULL AUTO_INCREMENT,
  `id_suratkeluar` int(11) DEFAULT NULL,
  `namas` varchar(50) DEFAULT NULL,
  `tempatlahirs` varchar(50) DEFAULT NULL,
  `tgllahirs` date DEFAULT NULL,
  `jks` varchar(50) DEFAULT NULL,
  `agamas` varchar(50) DEFAULT NULL,
  `pekerjaans` varchar(50) DEFAULT NULL,
  `dusuns` varchar(50) DEFAULT NULL,
  `desas` varchar(50) DEFAULT NULL,
  `kecs` varchar(50) DEFAULT NULL,
  `namai` varchar(50) DEFAULT NULL,
  `tempatlahiri` varchar(50) DEFAULT NULL,
  `tgllahiri` date DEFAULT NULL,
  `jki` varchar(50) DEFAULT NULL,
  `agamai` varchar(50) DEFAULT NULL,
  `pekerjaani` varchar(50) DEFAULT NULL,
  `dusuni` varchar(50) DEFAULT NULL,
  `desai` varchar(50) DEFAULT NULL,
  `keci` varchar(50) DEFAULT NULL,
  `oleh` varchar(50) DEFAULT NULL,
  `tglnikah` date DEFAULT NULL,
  PRIMARY KEY (`id_nikah`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `nikah` */

/*Table structure for table `penduduk` */

DROP TABLE IF EXISTS `penduduk`;

CREATE TABLE `penduduk` (
  `id_penduduk` int(10) NOT NULL AUTO_INCREMENT,
  `nik` varchar(50) DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `tempatlahir` varchar(50) DEFAULT NULL,
  `tgllahir` varchar(50) DEFAULT NULL,
  `jk` varchar(50) DEFAULT NULL,
  `dusun` varchar(50) DEFAULT NULL,
  `rt` varchar(50) DEFAULT NULL,
  `kel` varchar(50) DEFAULT NULL,
  `kecamatan` varchar(50) DEFAULT NULL,
  `agama` varchar(50) DEFAULT NULL,
  `statuskawin` varchar(50) DEFAULT NULL,
  `pekerjaan` varchar(50) DEFAULT NULL,
  `kwn` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_penduduk`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;

/*Data for the table `penduduk` */

insert  into `penduduk`(`id_penduduk`,`nik`,`nama`,`tempatlahir`,`tgllahir`,`jk`,`dusun`,`rt`,`kel`,`kecamatan`,`agama`,`statuskawin`,`pekerjaan`,`kwn`) values 
(1,'5203084107650570','NURHASANAH','BATUYANG','19-12-1960','PEREMPUAN','Padamara','01/04','Batuyang','Pringgabaya','ISLAM','CERAI MATI','PEDAGANG','WNI'),
(2,'5203087112580210','RABIQ','PADAMARA','31-12-1958','PEREMPUAN','Padamara','01/02','Batuyang','Pringgabaya','ISLAM','KAWIN','PNS','WNI'),
(3,'5203083112770230','ROSI','BAGEK LONGGEK','31-12-1977','Laki-Laki','Bagek Longgek','02/03','Batuyang','Pringgabaya','ISLAM','KAWIN','PETANI','WNI'),
(4,'5203080107601330','MUKTAR','BATUYANG','31-12-1965','Laki-Laki','Batuyang','01/02','Batuyang','Pringgabaya','ISLAM','BELUM KAWIN','PETANI','WNI'),
(5,'5203087112970080','MAESU WARNI SUSANTI','BAGEK LONGGEK','31-12-1997','PEREMPUAN','Bagek Longgek','01/02','Batuyang','Pringgabaya','ISLAM','BELUM KAWIN','Bidan','WNI'),
(6,'5203082348237343','NIPTAH','BATUYANG','31-12-1954','PEREMPUAN','BATUYANG','01/01','Batuyang','Pringgabaya','ISLAM','KAWIN','PEDAGANG','WNI'),
(7,'5203082343242534','RUSLAH','BATUYANG','08-09-1978','Laki-Laki','Batuyang','01/01','Batuyang','Pringgabaya','ISLAM','KAWIN','TIDAK BEKERJA','WNI'),
(8,'5203084325345345','ROSITA','BATUYANG','31-01-1985','Laki-Laki','Batuyang','01/01','Batuyang','Pringgabaya','ISLAM','KAWIN','TIDAK BEKERJA','WNI'),
(9,'5203087134534543','RUSMAN','BATUYANG','31-01-1985','Laki-Laki','Batuyang','01/01','Batuyang','Pringgabaya','ISLAM','KAWIN','WIRASWASTA','WNI'),
(10,'5203082345435234','SURYATIN','MAMBEN','31-12-1985','PEREMPUAN','Batuyang','01/01','Batuyang','Pringgabaya','ISLAM','KAWIN','WIRASWASTA','WNI'),
(11,'5203089975774343','NIKMA ELSAFATI','SELONG','17-01-2019','PEREMPUAN','Batuyang','01/01','Batuyang','Pringgabaya','ISLAM','BELUM KAWIN','TIDAK BEKERJA','WNI'),
(12,'5203080238472364','MARSUDIANTO','BATUYANG','20-04-1987','Laki-Laki','Batuyang','01/01','Batuyang','Pringgabaya','ISLAM','KAWIN','WIRASWASTA','WNI'),
(13,'5203080435345567','NORLIATI BINTI AKSITO','BATUYANG','19-04-1989','PEREMPUAN','Batuyang','02/02','Batuyang','Pringgabaya','ISLAM','KAWIN','TIDAK BEKERJA','WNI'),
(14,'5203080657689788','AMAQ MAHSUS','BATUYANG','16444','Laki-Laki','Batuyang','08/03','Batuyang','Pringgabaya','ISLAM','KAWIN','TIDAK BEKERJA','WNI'),
(15,'5203080346548594','INAQ MAHSUS','BATUYANG','01-07-1946','PEREMPUAN','Batuyang','08/03','Batuyang','Pringgabaya','ISLAM','KAWIN','TIDAK BEKERJA','WNI'),
(16,'5203080324623741','ALPANDI','BATUYANG','09-04-1994','Laki-Laki','Batuyang','01/05','Batuyang','Pringgabaya','ISLAM','BELUM KAWIN','TIDAK BEKERJA','WNI'),
(17,'5203080234283472','DIAN SAPUTRA','BATUYANG','27-12-2001','Laki-Laki','Batuyang','01/06','Batuyang','Pringgabaya','ISLAM','BELUM KAWIN','TIDAK BEKERJA','WNI'),
(18,'5203080746573494','SITI SARTINA','BATUYANG','30-09-1989','PEREMPUAN','Batuyang','02/07','Batuyang','Pringgabaya','ISLAM','BELUM KAWIN','TIDAK BEKERJA','WNI'),
(19,'5203080345878923','MAHIDI','BATUYANG','31-12-1970','Laki-Laki','Batuyang','01/08','Batuyang','Pringgabaya','ISLAM','BELUM KAWIN','TIDAK BEKERJA','WNI'),
(20,'5203080485733434','INAQ NURJAMINAH','BATUYANG','07-01-1969','PEREMPUAN','Batuyang','01/09','Batuyang','Pringgabaya','ISLAM','BELUM KAWIN','TIDAK BEKERJA','WNI'),
(21,'5203080433453457','KURNIAWAN','BATUYANG','10-04-2000','Laki-Laki','Batuyang','01/10','Batuyang','Pringgabaya','ISLAM','BELUM KAWIN','TIDAK BEKERJA','WNI'),
(22,'5203080345783450','ARIA PUTRA JAYANTA','BATUYANG','22-01-2009','Laki-Laki','Batuyang','01/11','Batuyang','Pringgabaya','ISLAM','BELUM KAWIN','TIDAK BEKERJA','WNI'),
(23,'5203080239848935','SALSA KAUTIN','BATUYANG','27-12-2007','PEREMPUAN','Batuyang','01/12','Batuyang','Pringgabaya','ISLAM','BELUM KAWIN','TIDAK BEKERJA','WNI'),
(24,'5203080238478345','APRIALIA','BATUYANG','02-04-2009','PEREMPUAN','Batuyang','01/13','Batuyang','Pringgabaya','ISLAM','BELUM KAWIN','TIDAK BEKERJA','WNI'),
(25,'5203080656690404','EFENDI','BATUYANG','04-04-1989','Laki-Laki','Batuyang','01/14','Batuyang','Pringgabaya','ISLAM','BELUM KAWIN','TIDAK BEKERJA','WNI'),
(26,'5203080123443779','SENEF','BATUYANG','01-07-1957','PEREMPUAN','Batuyang','01/15','Batuyang','Pringgabaya','ISLAM','BELUM KAWIN','TIDAK BEKERJA','WNI'),
(27,'5203080485745690','RUSDIN AMRIN','BATUYANG','27-07-1970','Laki-Laki','Batuyang','01/16','Batuyang','Pringgabaya','ISLAM','BELUM KAWIN','TIDAK BEKERJA','WNI'),
(28,'5203080854677333','SUHMIN','BATUYANG','31-12-1970','PEREMPUAN','Batuyang','01/17','Batuyang','Pringgabaya','ISLAM','BELUM KAWIN','TIDAK BEKERJA','WNI'),
(29,'5203080457435662','RUSMIATI LESTARI','BATUYANG','31-12-1970','PEREMPUAN','Batuyang','02/18','Batuyang','Pringgabaya','ISLAM','BELUM KAWIN','TIDAK BEKERJA','WNI'),
(30,'5203080974636242','HAIRUDIN','BATUYANG','29-05-1994','Laki-Laki','Batuyang','01/19','Batuyang','Pringgabaya','ISLAM','BELUM KAWIN','TIDAK BEKERJA','WNI'),
(31,'5203080214234623','HAILI','BATUYANG','01-07-1969','PEREMPUAN','Batuyang','06/20','Batuyang','Pringgabaya','ISLAM','BELUM KAWIN','TIDAK BEKERJA','WNI'),
(32,'5203080854634352','WIRA HAIDIN','BATUYANG','21-04-1998','Laki-Laki','Batuyang','01/21','Batuyang','Pringgabaya','ISLAM','BELUM KAWIN','TIDAK BEKERJA','WNI'),
(33,'5203080532762734','DEWI AJURA','BATUYANG','01-07-2005','PEREMPUAN','Batuyang','05/22','Batuyang','Pringgabaya','ISLAM','BELUM KAWIN','TIDAK BEKERJA','WNI'),
(34,'5203080423876427','USMAN','BATUYANG','31-12-1969','Laki-Laki','Batuyang','01/23','Batuyang','Pringgabaya','ISLAM','BELUM KAWIN','TIDAK BEKERJA','WNI'),
(35,'5203080292378348','ROH','BATUYANG','21-12-1970','PEREMPUAN','Batuyang','01/24','Batuyang','Pringgabaya','ISLAM','BELUM KAWIN','TIDAK BEKERJA','WNI'),
(36,'5203080898345763','ALDIEN ARUNDINA','BATUYANG','06-06-1991','Laki-Laki','Batuyang','01/25','Batuyang','Pringgabaya','ISLAM','BELUM KAWIN','TIDAK BEKERJA','WNI'),
(37,'5203080773457348','ALGIEBA ','BATUYANG','04-04-2010','Laki-Laki','Batuyang','01/26','Batuyang','Pringgabaya','ISLAM','BELUM KAWIN','TIDAK BEKERJA','WNI'),
(38,'5203080874676253','ARIFIN','BATUYANG','31-12-1960','Laki-Laki','Batuyang','01/27','Batuyang','Pringgabaya','ISLAM','BELUM KAWIN','TIDAK BEKERJA','WNI'),
(39,'5203080874676254','Siti Marlina','BATUYANG','01-10-1998','PEREMPUAN','Batuyang','01/01','Batuyang','Pringgabaya','ISLAM','BELUM KAWIN','SWASTA','WNI');

/*Table structure for table `suratkeluar` */

DROP TABLE IF EXISTS `suratkeluar`;

CREATE TABLE `suratkeluar` (
  `id_suratkeluar` int(11) NOT NULL AUTO_INCREMENT,
  `no_suratkeluar` varchar(50) DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `tgl_suratkeluar` date DEFAULT NULL,
  `kepada` varchar(50) DEFAULT NULL,
  `perihal` text,
  `jenis_surat` enum('Usaha','Nikah','Belum Nikah','Catatan Kepolisian','Bepergian','Kehilangan','Domisili','Meninggal Dunia') DEFAULT NULL,
  PRIMARY KEY (`id_suratkeluar`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `suratkeluar` */

insert  into `suratkeluar`(`id_suratkeluar`,`no_suratkeluar`,`nama`,`tgl_suratkeluar`,`kepada`,`perihal`,`jenis_surat`) values 
(1,'517 / 85  / Pemb /2019;','rosi','2019-12-01','Bank BCA','membuka usaha yaitu restorant di desa batuyang','Usaha'),
(2,'479 /Kesra/2019','Niptah','2019-12-12','kantor Dinas Catatan Sipil Selong.','untuk pembuatan akta anaknya di kantor dinas Capil Selong.','Nikah'),
(3,'145/Pem /2019','rosita','2019-12-14','Tempat Kerja','Lamaran Pekerjaan','Belum Nikah'),
(4,'301.T /Trantib /2019','Muktar','2019-12-14','perusahaan','Lamaran Pekerjaan ','Catatan Kepolisian'),
(5,'145  /  281  / UM  /2019','Alfandi','2019-12-14','Petugas Umroh','Pergi Umroh','Bepergian'),
(6,'145 /Trantb /2019;','Dian Saputra','2019-12-01','kantor kepolisian','Kehilangan Sepeda Motor','Kehilangan');

/*Table structure for table `suratmasuk` */

DROP TABLE IF EXISTS `suratmasuk`;

CREATE TABLE `suratmasuk` (
  `id_suratmasuk` int(11) NOT NULL AUTO_INCREMENT,
  `no_suratmasuk` varchar(50) DEFAULT NULL,
  `tgl_suratmasuk` date DEFAULT NULL,
  `tgl_terima` date DEFAULT NULL,
  `sifat` enum('Biasa','Penting','Sangat Penting','Rahasia') DEFAULT NULL,
  `asal` varchar(50) DEFAULT NULL,
  `perihal` text,
  `status_disposisi` enum('Belum','Sudah') DEFAULT NULL,
  `file` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_suratmasuk`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `suratmasuk` */

insert  into `suratmasuk`(`id_suratmasuk`,`no_suratmasuk`,`tgl_suratmasuk`,`tgl_terima`,`sifat`,`asal`,`perihal`,`status_disposisi`,`file`) values 
(1,'141/07/PMD/2019','2019-12-01','2019-12-01','Sangat Penting','kantor camat pringgabaya','Permintaan Data dan pengelolaan keuangan desa','Belum','doc-1576288352.pdf'),
(2,'460/06/PC/2019','2019-12-05','2019-12-05','Penting','camat pringgabaya','Pelaksanaan Program Prorsrasta thn.2019','Belum','doc-1576288584.pdf'),
(3,'0952/2019/PA.Sel','2019-12-09','2019-12-09','Penting','PA.Selong','Relaas Pemberitahuan','Belum','doc-1576288723.pdf'),
(4,'02/BPD/Batuyang/2019','2019-12-14','2019-12-14','Penting','Ketua BPD Desa Batuyang','Undangan','Belum','doc-1576288832.pdf');

/*Table structure for table `usaha` */

DROP TABLE IF EXISTS `usaha`;

CREATE TABLE `usaha` (
  `id_usaha` int(11) NOT NULL AUTO_INCREMENT,
  `id_suratkeluar` int(11) DEFAULT NULL,
  `nik` varchar(50) DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `jk` varchar(50) DEFAULT NULL,
  `tempatlahir` varchar(50) DEFAULT NULL,
  `tgllahir` date DEFAULT NULL,
  `agama` varchar(50) DEFAULT NULL,
  `statuskawin` varchar(50) DEFAULT NULL,
  `pekerjaan` varchar(50) DEFAULT NULL,
  `dusun` varchar(50) DEFAULT NULL,
  `rt` varchar(50) DEFAULT NULL,
  `kel` varchar(50) DEFAULT NULL,
  `kecamatan` varchar(50) DEFAULT NULL,
  `tahunusaha` int(11) DEFAULT NULL,
  `pinjamanbank` varchar(50) DEFAULT NULL,
  `ket` text,
  PRIMARY KEY (`id_usaha`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `usaha` */

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) DEFAULT NULL,
  `tanggallahir` date DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `jk` enum('laki-laki','perempuan') DEFAULT NULL,
  `level` enum('Admin','Kepala Desa') DEFAULT NULL,
  `foto` varchar(50) DEFAULT NULL,
  `tempatlahir` varchar(50) DEFAULT NULL,
  `alamat` text,
  `catatan` text,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `user` */

insert  into `user`(`id_user`,`nama`,`tanggallahir`,`email`,`password`,`jk`,`level`,`foto`,`tempatlahir`,`alamat`,`catatan`) values 
(2,'Siti Marlina','1998-10-01','sitimarlina0198@gmail.com','123698745','perempuan','Admin','fot-1576251582.png','BATUYANG','Jln. D.sm Development No. 99cinta','Jangan menyerah sebelum putus asa!!'),
(7,'Kepala Desa','1991-06-18','kepaladesa@gmail.com','kepaladesa','laki-laki','Kepala Desa','avatar5.png','Bagek Longgek',NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
