<?php
    include '../config/koneksi.php';  
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Lupa Password || Siades</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="../plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="#"><img src="../img/login.png" width="250" alt=""></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Lupa Password!</p>

    <form action="" method="post">
      <div class="form-group has-feedback">
        <label for="">Email:</label>
        <input type="email" name="email" class="form-control" placeholder="Email" required>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-12">
            <button type="submit" name="send" class="btn btn-primary btn-block ">Kirim Password</button>
        </div>
        <!-- /.col -->
        <!-- /.col -->
      </div>
    </form>
    <br>
    <a href="login.php">Halaman Login</a><br>
    
<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
  if(isset($_POST['send'])){
    $email = $_POST['email'];

    $sql = $koneksi->query("select * from user where email='$email'") or die(mysqli_error($koneksi));
    $ambilemail=$sql->num_rows;
    
if($ambilemail==1){      
  //Load Composer's autoloader
  $data= $sql->fetch_assoc();
  $email = $data['email'];
  $password = $data['password'];
require '../assets/phpmailer/vendor/autoload.php';
$mail = new PHPMailer(true);                              // Passing `true` enables exceptions
try {
    //Server settings
    // $mail->SMTPDebug = 1;                                 // Enable verbose debug output
    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'kantordesabatuyang@gmail.com';                 // SMTP username
    $mail->Password = 'bergek123';                           // SMTP password
    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 587;                                    // TCP port to connect to

    //Recipients
    $mail->setFrom('kantordesabatuyang@gmail.com', 'SIADES');
    $mail->addAddress($email);     // Add a recipient

    //Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'Lupa Password';
    $mail->Body    = 'Hai '.$email.', <br>
    Sistem kami telah menerima lupa password anda,<br>
    silahkan login kembali menggunakan password ini = ' . $password . '';
    $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

    $mail->send();
    echo '';
} catch (Exception $e) {
    echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
}

        echo '<div class="card">
                <div class="alert alert-success mb-0" id="pesan">
                   Password Sudah Dikirim Silahkan Cek Email Anda!!!
              </div>
            </div>';
    }else {
      echo '<div class="card">
                <div class="alert alert-danger mb-0" id="pesan">
                  Email Salah!!!
                </div>
            </div>';
    }
  }
?>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="../plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>
<script >
     $(document).ready(function(){setTimeout(function(){$("#pesan").fadeIn('slow');}, 500);});
      setTimeout(function(){$("#pesan").fadeOut('slow');}, 2000);
</script>
</body>
</html>
