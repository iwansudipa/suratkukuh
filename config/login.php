<?php
    ob_start();
    session_start();
    include '../config/koneksi.php';  

    if (isset($_SESSION['admin'])){
        header('location:../index.php');
        exit();
    }elseif (isset($_SESSION['kepaladesa'])) {
      header('location:../kepaladesa/index.php');
        exit();
    }else {
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Login Siades</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="../plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="#">
      <b>SI</b> | Surat Desa <br>
      <img src="../img/logokukuh.jpg" alt=""></a>
    
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Silahkan Login Terlebih Dahulu!</p>

    <form action="" method="post">
      <div class="form-group has-feedback">
        <input type="email" name="email" class="form-control" placeholder="Email" required>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input  class="form-control" type="password" id="myInput" name="password" placeholder="Password" required>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox"> Tampilkan Password
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" name="login" class="btn btn-primary btn-block btn-flat">Login</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
<?php
    if (isset($_POST['login'])) {
        $email=$_POST['email'];
        $password=$_POST['password'];
        $sql = $koneksi->query("select * from user where email='$email' and password='$password'") or die(mysqli_error($koneksi));
        $data = $sql->fetch_assoc();
        $cek = $sql->num_rows;

        if ($cek>=1) {
            if ($data['level']=="Admin") {
                $_SESSION['admin'] = $data['id_user'];
                header("location:../index.php");
            }elseif ($data['level']=="Kepala Desa") {
                $_SESSION['kepaladesa'] = $data['id_user'];
                header("location:../kepaladesa/index.php");
            }
        }else {
            echo"
              <div class='alert alert-danger alert-dismissible' id='pesan'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                <h4><i class='icon fa fa-ban'></i> Gagal Login!</h4>
                Kombinasi email dan password salah!!!.
              </div>
            "; 
        }
    }
?>
    <a href="lupapassword.php">Lupa password</a><br>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="../plugins/iCheck/icheck.min.js"></script>
<script>
function myFunction() {
  var x = document.getElementById("myInput");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}
</script>

<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });

    $('input').on('ifChecked', function(event){
      var x = document.getElementById("myInput");
        if (x.type === "password") {
          x.type = "text";
        }
    });

    $('input').on('ifUnchecked', function(event){
      var x = document.getElementById("myInput");
        if (x.type === "text") {
          x.type = "password";
        }
    });
  });
</script>
<script >
     $(document).ready(function(){
       setTimeout(function(){
         $("#pesan").fadeIn('slow');}, 500);

       setTimeout(function(){
         $("#pesan").fadeOut('slow');}, 2000);
        });
</script>
<script>

</script>

</body>
</html>
<?php
    }
?>
