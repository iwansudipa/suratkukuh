<?php
    $id = $_GET[id];
    $sql=$koneksi->query("select * from suratkeluar where id_suratkeluar='$id'");
    $data=$sql->fetch_assoc();
    $jenissurat=$data['jenis_surat'];
?>  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      Surat Keluar
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="?page=suratkeluar">Surat Keluar</a></li>
        <li class="active">Edit</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Data Surat Keluar</h3>
            </div>
            <form role="form" method="POST" enctype="multipart/form-data">
              <div class="box-body">
              <div class="row">
              <div class="col-lg-12" >
              <div class="alert alert-warning alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-warning"></i> Perhatian!</h4>
                Silahkan ganti isi form dibawah ini untuk mengubah data surat keluar yang salah
              </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label>No Surat:</label>
                  <input type="text" name="nosurat" class="form-control" placeholder="Masukkan No Surat" value="<?php echo $data['no_suratkeluar']?>" required>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Nama:</label>
                  <input type="text" name="nama" class="form-control" placeholder="Masukkan Nama" value="<?php echo $data['nama']?>" required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                <label>Tanggal Surat Keluar:</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" name="tglsurat" class="form-control pull-right" id="datepicker" value="<?php echo date('d-m-Y', strtotime($data['tgl_suratkeluar']));?>" placeholder="Masukkan Tanggal" required>
                </div>
                <!-- /.input group -->
              </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Perihal:</label>
                  <textarea name="perihal" class="form-control" rows="5" placeholder="Masukkan Perihal" required><?php echo $data['perihal']?></textarea>
              </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Kepada:</label>
                  <input type="text" name="kepada" class="form-control" placeholder="Masukkan Kepada Surat Keluar" value="<?php echo $data['kepada']?>" required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                <label>Pilih Jenis Surat:</label>
                <select class="form-control select2" style="width: 100%;" name="jenissurat" required>
                  <option selected="disable selected" value="">=>Pilih Surat Keterangan<=</option>
                  <option value="Usaha" <?php if ($jenissurat=="Usaha") {
                      echo "selected";
                    }?>>Usaha</option>
                  <option value="Nikah" <?php if ($jenissurat=="Nikah") {
                      echo "selected";
                    }?>>Nikah</option>
                  <option value="Belum Nikah" <?php if ($jenissurat=="Belum Nikah") {
                      echo "selected";
                    }?>>Belum Nikah</option>
                  <option value="Catatan Kepolisian" <?php if ($jenissurat=="Catatan Kepolisian") {
                      echo "selected";
                    }?>>Catatan Kepolisian</option>
                  <option value="Bepergian" <?php if ($jenissurat=="Bepergian") {
                      echo "selected";
                    }?>>Bepergian</option>
                  <option value="Kehilangan" <?php if ($jenissurat=="Kehilangan") {
                      echo "selected";
                    }?>>Kehilangan</option>
                  <option value="Domisili" <?php if ($jenissurat=="Domisili") {
                      echo "selected";
                    }?>>Domisili</option>
                  <option value="Meninggal Dunia" <?php if ($jenissurat=="Meninggal Dunia") {
                      echo "selected";
                    }?>>Meninggal Dunia</option>
                </select>
              </div>
              </div>
              </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" name="simpan" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                <a href="javascript:history.go(-1)" class="btn btn-default"><i class="fa fa-arrow-left"></i> Kembali</a>
              </div>
            </form>
<?php
if (isset($_POST['simpan'])) {
  $nosurat=$_POST['nosurat'];
  $nama=$_POST['nama'];
  $kepada=$_POST['kepada'];
  $tglsurat=date('Y-m-d', strtotime($_POST['tglsurat']));
  $perihal=$_POST['perihal'];
  $jenissurat=$_POST['jenissurat'];

  $sql = $koneksi->query("update suratkeluar set no_suratkeluar='$nosurat', nama='$nama', tgl_suratkeluar='$tglsurat', kepada='$kepada', perihal='$perihal', jenis_surat='$jenissurat' where id_suratkeluar='$id'") or die(mysqli_error($koneksi));
  if ($sql==true) {
    ?>
    <script>
      swal({
            title: 'Suksess!',
            text: 'Data Berhasil Diedit',
            type: 'success',
            html: true,
            confirmButtonClass: 'btn-primary',
            confirmButtonText: 'Ooke',
            },function(){
            window.location.href = "?page=suratkeluar"
          });
    </script>
  <?php
  } else {
    ?>
    <script>
      swal("Gagal!", "Terjadi kesalahan", "error");
    </script>
    <?php
  }           
}
?>  
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  