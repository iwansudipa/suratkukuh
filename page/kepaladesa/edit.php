<?php
    $id=$_GET[id_kepaladesa];
    $sql=$koneksi->query("select * from kepaladesa where id_kepaladesa='$id'");
    $data=$sql->fetch_assoc();
 ?> 
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Kepala Desa
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="?page=kepaladesa">Kepala Desa</a></li>
        <li class="active">Edit</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
    <div class="row">
    <div class="col-md-6">
        <!-- left column -->
        <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Kepala Desa</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data">
              <div class="box-body">
                <div class="form-group">

                  <label for="inputEmail3" class="col-sm-3 control-label">Nama Lengkap</label>

                  <div class="col-sm-9">
                    <input type="text" class="form-control" name="nama" placeholder="Masukkan Nama Lengkap" value="<?php echo $data['nama']?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-3 control-label">Tanda Tangan</label>

                  <div class="col-sm-9">
                    <img src="img/<?php echo $data['ttd']?>" alt="" width="300">
                    <br>
                    <br>
                    <label for="exampleInputFile">Ubah Tanda Tangan</label>
                    <input type="file" id="exampleInputFile" name="file" accept="image/jpeg, image/png">
                    <p class="help-block">Format file harus gambar (Jpeg dan png)!.</p>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-success" name="simpan"><i class="fa fa-save"></i> Simpan</button>
                <a href="javascript:history.go(-1)" class="btn btn-default"><i class="fa fa-arrow-left"></i> Kembali</a>
              </div>
              <!-- /.box-footer -->
            </form>
<?php
  if (isset($_POST['simpan'])){

    $nama=$_POST['nama'];
        $extensi = explode(".", $_FILES['file']['name']);
        $nama_file = "ttd-".round(microtime(true)).".".end($extensi);
        $format = pathinfo($nama_file, PATHINFO_EXTENSION);
        $file_asal = $_FILES['file']['tmp_name'];
        if (!empty($file_asal) && $format=="jpg" or "png") {
            $sql=$koneksi->query("select * from kepaladesa where id_kepaladesa='$id'");
            $data = $sql->fetch_assoc();
            $ubahfile = $data['ttd'];
            unlink("img/$ubahfile");
            $upload = move_uploaded_file($file_asal, 'img/'.$nama_file);
            $sql=$koneksi->query("update kepaladesa set nama='$nama', ttd='$nama_file' where id_kepaladesa='$id'");
            ?>
            <script>
                swal({
                    title: 'Suksess!',
                    text: 'Data Berhasil disimpan',
                    type: 'success',
                    html: true,
                    confirmButtonClass: 'btn-primary',
                    confirmButtonText: 'Ooke',
                    },function(){
                    window.location.href = "?page=kepaladesa"
                    });
            </script>
            <?php
        }elseif (empty($file_asal)) {
            $sql=$koneksi->query("update kepaladesa set nama='$nama' where id_kepaladesa='$id'");
            ?>
            <script>
                swal({
                    title: 'Suksess!',
                    text: 'Data Berhasil Diedit',
                    type: 'success',
                    html: true,
                    confirmButtonClass: 'btn-primary',
                    confirmButtonText: 'Ooke',
                    },function(){
                    window.location.href = "?page=kepaladesa"
                    });
            </script>
            <?php
        }else {
            ?>
            <script>
                swal("Gagal!", "Format file harus gambar (Jpeg dan png)!", "error");
            </script>
            <?php
        }
  }
?>
          </div>
    </div>
    <div class="col-md-6">
    <div class="alert alert-warning alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <h4><i class="icon fa fa-warning"></i> Perhatian!</h4>
      Dalam mengubah file gambar tanda tangan harus hitam putih (background putih) atau background transparan
    </div>
    </div>
    </div>
    </section>
    <!-- /.content -->
    
  </div>
  <!-- /.content-wrapper -->
  