<?php
  $sql = $koneksi->query("select * from kepaladesa");
  $data=$sql->fetch_assoc();
?>  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Kepala Desa
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Kepala Desa</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
        <div class="box box-solid">
            <!-- /.box-header -->
            <div class="box-body">
              <div class="box-group" id="accordion">
                <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                <div class="panel box box-primary">
                  <div class="box-header with-border">
                    <h4 class="box-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                        <?php echo $data['nama']?>
                      </a>
                    </h4>
                  </div>
                  <div id="collapseOne" class="panel-collapse collapse in">
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tr>
                                <th style="width:15%">Tanda Tangan :</th>
                                <td><img src="img/<?php echo $data['ttd']?>" alt="" width="300"></td>
                                </tr>
                                
                            </table>
                     </div>
                    </div>
                  </div>
                  
                </div>
                
              </div>
              <a href="?page=kepaladesa&aksi=edit&id_kepaladesa=<?php echo $data['id_kepaladesa']?>" class="btn btn-info"><i class="fa fa-edit"></i> Edit</a>
            </div>
            <!-- /.box-body -->
            
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
<script>
  $('.hapus').on('click',function(){
      var getLink = $(this).attr('href');
        swal({
              title: 'Hapus',
              text: 'Apakah anda yakin untuk menghapus data?',
              type: "warning",
              html: true,
              showCancelButton: true,
              confirmButtonClass: 'btn-danger',
              confirmButtonText: 'Hapus!',
              cancelButtonText: "Batal!",
              },function(){
              window.location.href = getLink
          });
      return false;
        });
</script>