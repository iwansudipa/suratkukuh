<?php
  $id = $_GET[id];
  $sql = $koneksi->query("select * from suratkeluar join belumnikah on suratkeluar.id_suratkeluar=belumnikah.id_suratkeluar
  where id_belumnikah='$id'") or mysqli_error($koneksi);
  $data = $sql->fetch_assoc();
  $agama=$data['agama'];

    if (isset($_POST['cetak'])) {
      $tujuan = strtoupper($_POST['tujuan']);
      $sql = $koneksi->query("update belumnikah set tujuan='$tujuan' where id_belumnikah='$id'") or die(mysqli_error($koneksi));
      if ($sql==true) {
        ?>
         <script>
            swal({
                title: 'Suksess!',
                text: 'Data Berhasil Diedit',
                type: 'success',
                html: true,
                confirmButtonClass: 'btn-primary',
                confirmButtonText: 'Ooke',
                },function(){
                window.location.href = "?page=belumnikah"
                });
        </script>
        <?php 
      }
    }   
?>  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      Belum Nikah
        <small>Surat Keterangan</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="?page=belumnikah">Keterangan Belum Nikah</a></li>
        <li class="active">Edit</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Data Surat Keterangan Belum Nikah</h3>
            </div>
            <form role="form" method="POST">
              <div class="box-body">
              <div class="row">
              <div class="col-lg-12">
                <div class="text-center">
                    <label style="font-size:18pt;">Nomor Surat: <?php echo $data['no_suratkeluar']?></label>
                </div>
              </div>
              <br>
              <br>
              <br>
              <div class="col-md-6">
              <div class="form-group">
                  <label>NIK:</label>
                  <input type="text" name="nik" class="form-control" placeholder="Masukkan NIK" pattern="[0-9]+" title="Hanya berupa angka" 
                  value="<?php echo $data['nik']?>" readonly required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Nama:</label>
                  <input type="text" name="nama" class="form-control" style="text-transform: uppercase" placeholder="Masukkan Nama" 
                  value="<?php echo $data['nama']?>" readonly required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Jenis Kelamin:</label>
                  <input type="text" name="jk" class="form-control" style="text-transform: uppercase" placeholder="Masukkan Nama" 
                  value="<?php echo $data['jk']?>" readonly required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Tempat Lahir:</label>
                  <input type="text" name="tempatlahir" class="form-control" style="text-transform: capitalize" placeholder="Masukkan Tempat Lahir"
                  value="<?php echo $data['tempatlahir']?>" readonly required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                <label>Tanggal Lahir:</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" name="tgllahir" class="form-control pull-right" placeholder="Masukkan Tanggal Lahir" 
                  value="<?php echo date('d-m-Y', strtotime($data['tgllahir']))?>" readonly required>
                </div>
                <!-- /.input group -->
              </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Agama:</label>
                  <input type="text" name="agama" class="form-control" style="text-transform: capitalize" placeholder="Masukkan Agama"
                  value="<?php echo $data['agama']?>" readonly required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Dusun:</label>
                  <input type="text" name="dusun" value="<?php echo $data['dusun']?>" class="form-control" style="text-transform: capitalize" placeholder="Masukkan Alamat Dusun" readonly required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>RT/RW:</label>
                  <input type="text" name="rt" value="<?php echo $data['rt']?>" class="form-control" style="text-transform: capitalize" placeholder="Masukkan Alamat RT/RW" readonly required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Kel/Desa:</label>
                  <input type="text" name="kel" value="<?php echo $data['kel']?>" class="form-control" style="text-transform: capitalize" placeholder="Masukkan Alamat Kelurahan" readonly required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Kecamatan:</label>
                  <input type="text" name="kecamatan" value="<?php echo $data['kecamatan']?>" class="form-control" style="text-transform: capitalize" placeholder="Masukkan Alamat Kecamatan" readonly required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Pekerjaan:</label>
                  <input type="text" name="pekerjaan" value="<?php echo $data['pekerjaan']?>" class="form-control" style="text-transform: capitalize" placeholder="Masukkan Pekerjaan" readonly required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Tujuan:</label>
                  <input type="text" name="tujuan" class="form-control" style="text-transform: capitalize" placeholder="Masukkan Tujuan Buat Surat" 
                  value="<?php echo $data['tujuan']?>" required>
                </div>
              </div>

              </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" name="cetak" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                <a href="javascript:history.go(-1)" class="btn btn-default"><i class="fa fa-arrow-left"></i> Kembali</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  