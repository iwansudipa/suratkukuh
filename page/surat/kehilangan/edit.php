<?php
 $id = $_GET[id];
 $sql = $koneksi->query("select * from suratkeluar join kehilangan on suratkeluar.id_suratkeluar=kehilangan.id_suratkeluar
 where id_kehilangan='$id'") or mysqli_error($koneksi);
 $data = $sql->fetch_assoc();
 $agama=$data['agama'];

    if (isset($_POST['simpan'])) {
      $kehilangan = ucwords($_POST['kehilangan']);
      $ket = ucwords($_POST['ket']);
    
      $sql = $koneksi->query(" update kehilangan set kehilangan='$kehilangan', ket='$ket' where id_kehilangan='$id'") or die(mysqli_error($koneksi));
      if ($sql==true) {
        ?>
        <script>
            swal({
                title: 'Suksess!',
                text: 'Data Berhasil Diedit',
                type: 'success',
                html: true,
                confirmButtonClass: 'btn-primary',
                confirmButtonText: 'Ooke',
                },function(){
                window.location.href = "?page=kehilangan"
                });
        </script>
        <?php 
      }else {
        ?>
        <script>
            swal($sql);
        </script>
        <?php 
      }
    }   
?>  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      Kehilangan
        <small>Surat Keterangan</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="?page=kehilangan">Keterangan Kehilangan</a></li>
        <li class="active">Tambah</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah Data Surat Keterangan Kehilangan</h3>
            </div>
            <form role="form" method="POST">
              <div class="box-body">
              <div class="row">
              <div class="col-lg-12">
                <div class="text-center">
                    <label style="font-size:18pt;">Nomor Surat: <?php echo $data['no_suratkeluar']?></label>
                </div>
              </div>
              <br>
              <br>
              <br>
              <div class="col-md-6">
              <div class="form-group">
                  <label>NIK:</label>
                  <input type="text" name="nik" class="form-control" placeholder="Masukkan NIK" pattern="[0-9]+" title="Hanya berupa angka"
                   value="<?php echo $data['nik']?>" readonly required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Nama:</label>
                  <input type="text" name="nama" class="form-control" style="text-transform: uppercase" placeholder="Masukkan Nama" 
                  value="<?php echo $data['nama']?>" readonly required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Tempat Lahir:</label>
                  <input type="text" name="tempatlahir" class="form-control" style="text-transform: capitalize" placeholder="Masukkan Tempat Lahir"
                  value="<?php echo $data['tempatlahir']?>" readonly required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                <label>Tanggal Lahir:</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" name="tgllahir" class="form-control pull-right" id="" placeholder="Masukkan Tanggal Lahir" 
                  value="<?php echo date('d-m-Y', strtotime($data['tgllahir']))?>" readonly required>
                </div>
                <!-- /.input group -->
              </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Jenis Kelamin:</label>
                  <input type="text" name="jk" class="form-control" style="text-transform: capitalize" placeholder="Masukkan Tempat Lahir"
                  value="<?php echo $data['jk']?>" readonly required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Agama:</label>
                  <input type="text" name="agama" class="form-control" style="text-transform: capitalize" placeholder="Masukkan Tempat Lahir"
                  value="<?php echo $data['agama']?>" readonly required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Pekerjaan:</label>
                  <input type="text" name="pekerjaan" class="form-control" style="text-transform: capitalize" placeholder="Masukkan Pekerjaan" 
                  value="<?php echo $data['pekerjaan']?>" readonly required>
                </div>
              </div>
              
              <div class="col-md-6">
              <div class="form-group">
                  <label>Dusun:</label>
                  <input type="text" name="dusun" id="dusun" class="form-control" style="text-transform: capitalize" placeholder="Masukkan Dusun" 
                  value="<?php echo $data['dusun']?>" readonly required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>RT/RW:</label>
                  <input type="text" name="rt" id="rt" class="form-control" style="text-transform: capitalize" 
                  value="<?php echo $data['rt']?>" placeholder="Masukkan RT/RW" readonly required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Kel/Desa</label>
                  <input type="text" name="kel" id="kel" class="form-control" style="text-transform: capitalize" 
                  value="<?php echo $data['kel']?>"  placeholder="Masukkan Kel/Desa" readonly required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Kecamatan :</label>
                  <input type="text" name="kec" id="kecamatan" class="form-control" style="text-transform: capitalize" 
                  value="<?php echo $data['kec']?>"  placeholder="Masukkan Kecamatan" readonly required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Kehilangan:</label>
                  <input type="text" name="kehilangan" class="form-control" style="text-transform: capitalize" placeholder="Masukkan Kehilangan"
                  value="<?php echo $data['kehilangan']?>" required>
                </div>
              </div>            
              <div class="col-md-12">
              <div class="form-group">
                  <label>Keterangan Kehilangan:</label>
                  <textarea name="ket" class="form-control" rows="4" style="text-transform: capitalize" placeholder="Masukkan Keterangan Kehilangan" required><?php echo $data['ket']?></textarea>
              </div>
              </div> 

              </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" name="simpan" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                <a href="javascript:history.go(-1)" class="btn btn-default"><i class="fa fa-arrow-left"></i> Kembali</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  