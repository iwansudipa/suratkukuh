<?php
  $id = $_GET[id];
  $sql = $koneksi->query("select * from suratkeluar where id_suratkeluar='$id'");
  $data = $sql->fetch_assoc();

    if (isset($_POST['cetak'])) {
      $nik = $_POST['nik'];
      $nama= strtoupper($_POST['nama']);
      $tempatlahir = ucwords($_POST['tempatlahir']);
      $tgllahir=date('Y-m-d', strtotime($_POST['tgllahir']));
      $jk = $_POST['jk'];      
      $agama = $_POST['agama'];
      $kwn = ucwords($_POST['kwn']);
      $pekerjaan = ucwords($_POST['pekerjaan']);
      $dusun = ucwords($_POST['dusun']);
      $rt = ucwords($_POST['rt']);
      $kel = ucwords($_POST['kel']);
      $kec = ucwords($_POST['kec']);
      $tujuan = ucwords($_POST['tujuan']);
      $tglberangkat=date('Y-m-d', strtotime($_POST['tglberangkat']));
      $transportasi = $_POST['transportasi'];
      $lamanya = ucwords($_POST['lamanya']);
      $ket = ucwords($_POST['ket']);
    
      $sql = $koneksi->query("insert into bepergian (id_suratkeluar, nik, nama, tempatlahir, tgllahir, jk, agama, kwn, pekerjaan, dusun, rt, kel, kec, tujuan, tglberangkat,
      transportasi, lamanya, ket)
      values('$id','$nik','$nama','$tempatlahir','$tgllahir','$jk','$agama','$kwn','$pekerjaan','$dusun','$rt','$kel','$kec','$tujuan', '$tglberangkat','$transportasi','$lamanya','$ket')") or die(mysqli_error($koneksi));
      if ($sql==true) {
        ?>
        <script>
          window.open("./laporan/suratketerangan/bepergian/bepergian.php");
          window.location.href="?page=bepergian";
        </script>
        <?php 
      }else {
        ?>
        <script>
            swal($sql);
        </script>
        <?php 
      }
    }   
?>  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      Bepergian
        <small>Surat Keterangan</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="?page=bepergian">Keterangan Bepergian</a></li>
        <li class="active">Tambah</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah Data Surat Keterangan Bepergian</h3>
            </div>
            <form role="form" method="POST">
              <div class="box-body">
              <div class="row">
              <div class="col-lg-12">
                <div class="text-center">
                    <label style="font-size:18pt;">Nomor Surat: <?php echo $data['no_suratkeluar']?></label>
                </div>
              </div>
              <br>
              <br>
              <br>
              <div class="col-md-6">
              <div class="form-group">
              <label>NIK:</label>
              <div class="input-group">
                
                <!-- /btn-group -->
                <input type="text" class="form-control" name="nik" id="nik" placeholder="Masukkan NIK" readonly required>
                <div class="input-group-btn">
                  <button type="button" data-toggle="modal" data-target="#modal-default" class="btn btn-info" >Cari</button>
                </div>
              </div>
              </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Nama:</label>
                  <input type="text" name="nama" id="nama" class="form-control" style="text-transform: uppercase" placeholder="Masukkan Nama" readonly required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Tempat Lahir:</label>
                  <input type="text" name="tempatlahir" id="tempatlahir" class="form-control" style="text-transform: capitalize" placeholder="Masukkan Tempat Lahir" readonly required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                <label>Tanggal Lahir:</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" name="tgllahir" id="tgllahir" class="form-control pull-right" id="" placeholder="Masukkan Tanggal Lahir" readonly required>
                </div>
                <!-- /.input group -->
              </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Jenis Kelamin:</label>
                  <input type="text" name="jk" id="jk" class="form-control" style="text-transform: capitalize" placeholder="Masukkan Jenis Kelamin" readonly required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Agama:</label>
                  <input type="text" name="agama" id="agama" class="form-control" style="text-transform: capitalize" placeholder="Masukkan Agama" readonly required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Dusun:</label>
                  <input type="text" name="dusun" id="dusun" class="form-control" style="text-transform: capitalize" placeholder="Masukkan Dusun" readonly required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>RT/RW:</label>
                  <input type="text" name="rt" id="rt" class="form-control" style="text-transform: capitalize" placeholder="Masukkan RT/RW" readonly required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Kel/Desa</label>
                  <input type="text" name="kel" id="kel" class="form-control" style="text-transform: capitalize" placeholder="Masukkan Kel/Desa" readonly required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Kecamatan :</label>
                  <input type="text" name="kec" id="kecamatan" class="form-control" style="text-transform: capitalize" placeholder="Masukkan Kecamatan" readonly required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Kewarganegaraan:</label>
                  <input type="text" name="kwn" id="kwn" class="form-control" style="text-transform: capitalize" placeholder="Masukkan Kewarganegaraan" readonly required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Pekerjaan:</label>
                  <input type="text" name="pekerjaan" id="pekerjaan" class="form-control" style="text-transform: capitalize" placeholder="Masukkan Pekerjaan" readonly required>
                </div>
              </div>
            
              <div class="col-md-6">
              <div class="form-group">
                  <label>Tujuan:</label>
                  <input type="text" name="tujuan" class="form-control" style="text-transform: capitalize" placeholder="Masukkan Tujuan" required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                <label>Tanggal Berangkat:</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" name="tglberangkat" class="form-control pull-right" id="datepicker" placeholder="Masukkan Tanggal Berangkat" required>
                </div>
                <!-- /.input group -->
              </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                <label>Transportasi:</label>
                <select class="form-control select2" style="width: 100%;" name="transportasi" required>
                  <option value="">=>Pilih Transportasi<=</option>
                  <option value="Pesawat">Pesawat</option>
                  <option value="Kapal Laut">Kapal Laut</option>
                </select>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Lamanya:</label>
                  <input type="text" name="lamanya" class="form-control" style="text-transform: capitalize" placeholder="Lamanya bepergian" required>
                </div>
              </div>
              <div class="col-md-12">
              <div class="form-group">
                  <label>Keterangan Lain-lain:</label>
                  <textarea name="ket" class="form-control" rows="4" style="text-transform: capitalize" placeholder="Masukkan Keterangan Lainnya" required></textarea>
              </div>
              </div> 

              </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" name="cetak" class="btn btn-success"><i class="fa fa-print"></i> Cetak</button>
                <a href="javascript:history.go(-1)" class="btn btn-default"><i class="fa fa-arrow-left"></i> Kembali</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <!-- /.cari penduduk -->
<div class="modal fade " id="modal-default">
  <div class="modal-dialog  modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title modal-primary">Cari Data Penduduk</h4>
      </div>
      <div class="modal-body">
        <div class="box box-primary">
            <!-- /.box-header -->
            <div class="box-body">
            <table id="lookup" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>NIK</th>
                  <th>Nama</th>
                  <th>Tempat/Tgl Lahir</th>
                  <th>Jenis Kelamin</th>
                  <th>Dusun</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
<?php
  $no = 1;
  $sql = $koneksi->query("select * from penduduk");
  while ($data=$sql->fetch_assoc()){
?>
                <tr>
                  <td><?php echo $no++;?></td>
                  <td><?php echo $data['nik'];?></td>
                  <td><?php echo $data['nama'];?></td>
                  <td><?php echo $data['tempatlahir']?>/<?php echo date('d-m-Y', strtotime($data['tgllahir']));?></td>
                  <td><?php echo $data['jk'];?></td>
                  <td><?php echo $data['dusun'];?></td>
                  <td class="text-center pilih" data-nik="<?php echo $data['nik']; ?>"  data-nama="<?php echo $data['nama']; ?>" data-jk="<?php echo $data['jk']; ?>"
                  data-tempatlahir="<?php echo $data['tempatlahir']; ?>" data-tgllahir="<?php echo $data['tgllahir']; ?>" data-agama="<?php echo $data['agama']; ?>"
                  data-statuskawin="<?php echo $data['statuskawin']; ?>" data-pekerjaan="<?php echo $data['pekerjaan']; ?>" data-dusun="<?php echo $data['dusun']; ?>"
                  data-rt="<?php echo $data['rt']; ?>" data-kel="<?php echo $data['kel']; ?>" data-kecamatan="<?php echo $data['kecamatan']; ?>" data-kwn="<?php echo $data['kwn']; ?>">
                    <button class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" title="Pilih"><i class="fa fa-check"></i></button>
                  </td>
                </tr>
<?php
  }
?>
                </tbody>
                <tfoot>
                <tr>
                <th>No</th>
                  <th>NIK</th>
                  <th>Nama</th>
                  <th>Tempat/Tgl Lahir</th>
                  <th>Jenis Kelamin</th>
                  <th>Dusun</th>
                  <th>Aksi</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- /.cari penduduk -->
<script>
  $(document).on('click', '.pilih', function (e) {
                document.getElementById("nik").value = $(this).attr('data-nik');
                document.getElementById("nama").value = $(this).attr('data-nama');
                document.getElementById("jk").value = $(this).attr('data-jk');
                document.getElementById("tempatlahir").value = $(this).attr('data-tempatlahir');
                document.getElementById("tgllahir").value = $(this).attr('data-tgllahir');
                document.getElementById("agama").value = $(this).attr('data-agama');
                document.getElementById("dusun").value = $(this).attr('data-dusun');
                document.getElementById("rt").value = $(this).attr('data-rt');
                document.getElementById("kel").value = $(this).attr('data-kel');
                document.getElementById("kecamatan").value = $(this).attr('data-kecamatan');
                document.getElementById("kwn").value = $(this).attr('data-kwn');
                document.getElementById("pekerjaan").value = $(this).attr('data-pekerjaan');
                

                $('#modal-default').modal('hide');
            });
            $(function () {
                $("#lookup").dataTable();
            });

</script>
  