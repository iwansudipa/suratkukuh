  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Bepergian
        <small>Surat Keterangan</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Surat Keterangan</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">Data Surat Keterangan Bepergian</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped table-responsive">
                <thead>
                <tr>
                  <th>No</th>
                  <th>No Surat</th>
                  <th>NIK</th>
                  <th>Nama</th>
                  <th>Tempat/Tgl. Lahir</th>
                  <th>Agama</th>
                  <th>Pekerjaan</th>
                  <th>Tujuan</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
<?php
  $no = 1;
  $sql = $koneksi->query("select * from suratkeluar join bepergian on suratkeluar.id_suratkeluar=bepergian.id_suratkeluar");
  while ($data=$sql->fetch_assoc()){
?>
                <tr>
                  <td><?php echo $no++;?></td>
                  <td><?php echo $data['no_suratkeluar'];?></td>
                  <td><?php echo $data['nik'];?></td>
                  <td><?php echo $data['nama'];?></td>
                  <td><?php echo $data['tempatlahir']?>/<?php echo date('d-m-Y', strtotime($data['tgllahir']));?></td>
                  <td><?php echo $data['agama'];?></td>
                  <td><?php echo $data['pekerjaan'];?></td>
                  <td><?php echo $data['tujuan'];?></td>
                  <td class="text-center">
                    <a href="?page=bepergian&aksi=edit&id=<?php echo $data['id_bepergian'];?>" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></a>
                    <a href="?page=bepergian&aksi=hapus&id=<?php echo $data['id_bepergian'];?>" class="btn btn-danger btn-xs hapus" data-toggle="tooltip" data-placement="top" title="Hapus"><i class="fa fa-trash"></i></a>
                    <a href="./laporan/suratketerangan/bepergian/idbepergian.php?id=<?php echo $data['id_bepergian'];?>" target="_blank" data-toggle="tooltip" data-placement="top" title="Cetak" class="btn btn-success btn-xs" ><i class="fa fa-print"></i></a>
                  </td>
                </tr>
<?php
  }
?>
                </tbody>
                <tfoot>
                <tr>
                  <th>No</th>
                  <th>No Surat</th>
                  <th>NIK</th>
                  <th>Nama</th>
                  <th>Tempat/Tgl. Lahir</th>
                  <th>Agama</th>
                  <th>Pekerjaan</th>
                  <th>Tujuan</th>
                  <th>Aksi</th>
                </tr>
                </tfoot>
              </table>
              <button type="button" data-toggle="modal" data-target="#modal-default" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah</button>
              <a href="javascript:history.go(-1)" class="btn btn-default"><i class="fa fa-arrow-left"></i> Kembali</a>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
      </div>
<!-- /.tambah -->
<div class="modal fade " id="modal-default">
  <div class="modal-dialog modal-lg">
    <div class="modal-content ">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title modal-primary">Pilih Data Surat Keluar</h4>
      </div>
      <div class="modal-body">
        <div class="box box-primary">
            <!-- /.box-header -->
            <div class="box-body">
              <table id="lookup" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>No Surat</th>
                  <th>Nama</th>
                  <th>Tgl. Surat</th>
                  <th>Kepada</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
<?php
  $no = 1;
  $sql = $koneksi->query("select * from suratkeluar where jenis_surat='Bepergian'");
  while ($data=$sql->fetch_assoc()){
?>
                <tr>
                  <td><?php echo $no++;?></td>
                  <td><?php echo $data['no_suratkeluar'];?></td>
                  <td><?php echo $data['nama'];?></td>
                  <td><?php echo date('d-m-Y', strtotime($data['tgl_suratkeluar']));?></td>
                  <td><?php echo $data['kepada'];?></td>
                  <td class="text-center">
                    <a href="?page=bepergian&aksi=tambah&id=<?php echo $data['id_suratkeluar'];?>" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" title="Pilih"><i class="fa fa-check"></i></a>
                  </td>
                </tr>
<?php
  }
?>
                </tbody>
                <tfoot>
                <tr>
                  <th>No</th>
                  <th>No Surat</th>
                  <th>Nama</th>
                  <th>Tgl. Surat</th>
                  <th>Kepada</th>
                  <th>Aksi</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- /.tambah -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<script>
  $(function () {
    $('#lookup').DataTable();
  });
</script>
<script>
  $('.hapus').on('click',function(){
      var getLink = $(this).attr('href');
        swal({
              title: 'Hapus',
              text: 'Apakah anda yakin untuk menghapus data?',
              type: "warning",
              html: true,
              showCancelButton: true,
              confirmButtonClass: 'btn-danger',
              confirmButtonText: 'Hapus!',
              cancelButtonText: "Batal!",
              },function(){
              window.location.href = getLink
          });
      return false;
        });
</script>
