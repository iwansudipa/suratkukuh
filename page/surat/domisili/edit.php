<?php
 $id = $_GET[id];
 $sql = $koneksi->query("select * from suratkeluar join domisili on suratkeluar.id_suratkeluar=domisili.id_suratkeluar
 where id_domisili='$id'") or mysqli_error($koneksi);
 $data = $sql->fetch_assoc();
 $agama=$data['agama'];

    if (isset($_POST['simpan'])) {
      $nik = $_POST['nik'];
      $nama= strtoupper($_POST['nama']);
      $tempatlahir = ucwords($_POST['tempatlahir']);
      $tgllahir=date('Y-m-d', strtotime($_POST['tgllahir']));
      $jk = $_POST['jk'];      
      $agama = $_POST['agama'];
      $pekerjaan = ucwords($_POST['pekerjaan']);
      $status = $_POST['status'];
      $dusun = ucwords($_POST['dusun']);
      $desa = ucwords($_POST['desa']);
      $kec = ucwords($_POST['kec']);
      $kab = ucwords($_POST['kab']);
    
      $sql = $koneksi->query("update domisili set nik='$nik', nama='$nama', tempatlahir='$tempatlahir', tgllahir='$tgllahir', jk='$jk',
      agama='$agama', pekerjaan='$pekerjaan', status='$status', dusun='$dusun', desa='$desa', kec='$kec', kab='$kab' where id_domisili='$id'") or die(mysqli_error($koneksi));
      if ($sql==true) {
        ?>
        <script>
            swal({
                title: 'Suksess!',
                text: 'Data Berhasil Diedit',
                type: 'success',
                html: true,
                confirmButtonClass: 'btn-primary',
                confirmButtonText: 'Ooke',
                },function(){
                window.location.href = "?page=domisili"
                });
        </script>
        <?php 
      }else {
        ?>
        <script>
            swal($sql);
        </script>
        <?php 
      }
    }   
?>  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      Domisili
        <small>Surat Keterangan</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="?page=domisili">Keterangan Domisili</a></li>
        <li class="active">Edit</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Data Surat Keterangan Domisili</h3>
            </div>
            <form role="form" method="POST">
              <div class="box-body">
              <div class="row">
              <div class="col-lg-12">
                <div class="text-center">
                    <label style="font-size:18pt;">Nomor Surat: <?php echo $data['no_suratkeluar']?></label>
                </div>
              </div>
              <br>
              <br>
              <br>
              <div class="col-md-6">
              <div class="form-group">
                  <label>NIK:</label>
                  <input type="text" name="nik" class="form-control" placeholder="Masukkan NIK" pattern="[0-9]+" title="Hanya berupa angka" 
                  value="<?php echo $data['nik']?>" required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Nama:</label>
                  <input type="text" name="nama" class="form-control" style="text-transform: uppercase" placeholder="Masukkan Nama" 
                  value="<?php echo $data['nama']?>"  required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Tempat Lahir:</label>
                  <input type="text" name="tempatlahir" class="form-control" style="text-transform: capitalize" placeholder="Masukkan Tempat Lahir" 
                  value="<?php echo $data['tempatlahir']?>"  required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                <label>Tanggal Lahir:</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" name="tgllahir" class="form-control pull-right" id="datepicker2" placeholder="Masukkan Tanggal Lahir"
                  value="<?php echo $data['tgllahir']?>"   required>
                </div>
                <!-- /.input group -->
              </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                <label>Jenis Kelamin:</label>
                <select class="form-control select2" style="width: 100%;" name="jk" required>
                  <option value="">=>Jenis Kelamin<=</option>
                  <option value="Laki-laki" <?php if ($data['jk']=="Laki-laki") {
                      echo "selected";
                  }?>>Laki-laki</option>
                  <option value="Perempuan"  <?php if ($data['jk']=="Perempuan") {
                      echo "selected";
                  }?>>Perempuan</option>
                </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                <label>Agama:</label>
                <select class="form-control select2" style="width: 100%;" name="agama" required>
                  <option value="">=>Pilih Agama<=</option>
                  <option value="Islam" <?php if ($agama=="Islam") {
                      echo "selected";
                  }?>>Islam</option>
                  <option value="Hindu" <?php if ($agama=="Hindu") {
                      echo "selected";
                  }?>>Hindu</option>
                  <option value="Budha" <?php if ($agama=="Budha") {
                      echo "selected";
                  }?>>Budha</option>
                  <option value="Kristen" <?php if ($agama=="Kristen") {
                      echo "selected";
                  }?>>Kristen</option>
                </select>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Pekerjaan:</label>
                  <input type="text" name="pekerjaan" class="form-control" style="text-transform: capitalize" placeholder="Masukkan Pekerjaan" 
                  value="<?php echo $data['pekerjaan']?>" required>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                <label>Status:</label>
                <select class="form-control select2" style="width: 100%;" name="status" required>
                  <option value="">=>Pilih Status<=</option>
                  <option value="Kawin" <?php if ($data['status']=="Kawin") {
                      echo "selected";
                  }?>>Kawin</option>
                  <option value="Belum Kawin" <?php if ($data['status']=="Belum Kawin") {
                      echo "selected";
                  }?>>Belum Kawin</option>
                </select>
                </div>
              </div>         
              <div class="col-md-6">
              <div class="form-group">
                  <label>Dusun:</label>
                  <input type="text" name="dusun" class="form-control" style="text-transform: capitalize" placeholder="Masukkan Alamat Dusun" 
                  value="<?php echo $data['dusun']?>" required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Desa:</label>
                  <input type="text" name="desa" class="form-control" style="text-transform: capitalize" placeholder="Masukkan Alamat Desa" 
                  value="<?php echo $data['desa']?>" required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Kecamatan:</label>
                  <input type="text" name="kec" class="form-control" style="text-transform: capitalize" placeholder="Masukkan Alamat Kecamatan"
                  value="<?php echo $data['kec']?>" required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Kabupaten:</label>
                  <input type="text" name="kab" class="form-control" style="text-transform: capitalize" placeholder="Masukkan Alamat Kabupaten"
                  value="<?php echo $data['kab']?>" required>
                </div>
              </div>

              </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" name="simpan" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                <a href="javascript:history.go(-1)" class="btn btn-default"><i class="fa fa-arrow-left"></i> Kembali</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  