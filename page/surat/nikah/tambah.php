<?php
  $id = $_GET[id];
  $sql = $koneksi->query("select * from suratkeluar where id_suratkeluar='$id'");
  $data = $sql->fetch_assoc();

    if (isset($_POST['cetak'])) {
      $namas = ucwords($_POST['namas']);
      $tempatlahirs = strtoupper($_POST['tempatlahirs']);
      $tgllahirs = date('Y-m-d', strtotime($_POST['tgllahirs']));
      $jks = "Laki-Laki";
      $agamas = $_POST['agamas'];
      $pekerjaans = ucwords($_POST['pekerjaans']);
      $dusuns = strtoupper($_POST['dusuns']);
      $desas = strtoupper($_POST['desas']);
      $kecs = ucwords($_POST['kecamatans']);
      
      $namai = ucwords($_POST['namai']);
      $tempatlahiri = strtoupper($_POST['tempatlahiri']);
      $tgllahiri = date('Y-m-d', strtotime($_POST['tgllahiri']));
      $jki = "PEREMPUAN";
      $agamai = $_POST['agamai'];
      $pekerjaani = ucwords($_POST['pekerjaani']);
      $dusuni = strtoupper($_POST['dusuni']);
      $desai = strtoupper($_POST['desai']);
      $keci = ucwords($_POST['kecamatani']);


      $oleh = ucwords($_POST['oleh']);
      $tglnikah = date('Y-m-d', strtotime($_POST['tglnikah']));

      $sql = $koneksi->query("insert into nikah (id_suratkeluar, namas, tempatlahirs, tgllahirs, jks, agamas, pekerjaans, dusuns, desas, kecs,
      namai, tempatlahiri, tgllahiri, jki, agamai, pekerjaani, dusuni, desai, keci, oleh, tglnikah)
      values('$id','$namas','$tempatlahirs','$tgllahirs','$jks','$agamas','$pekerjaans','$dusuns','$desas','$kecs',
      '$namai','$tempatlahiri','$tgllahiri','$jki','$agamai','$pekerjaani','$dusuni','$desai','$keci','$oleh','$tglnikah')") or die(mysqli_error($koneksi));
      if ($sql==true) {
        ?>
        <script>
          window.open("./laporan/suratketerangan/nikah/nikah.php");
          window.location.href="?page=nikah";
        </script>
        <?php 
      }
    }   
?>  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Nikah
        <small>Surat Keterangan</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="?page=nikah">Keterangan Nikah</a></li>
        <li class="active">Tambah</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah Data Surat Keterangan Nikah</h3>
            </div>
            <form role="form" method="POST">
              <div class="box-body">
              <div class="row">
              <div class="col-lg-12">
                <div class="text-center">
                    <label style="font-size:18pt;">Nomor Surat: <?php echo $data['no_suratkeluar']?></label>
                </div>
              </div>
              <br>
              <br>
              <br>
              <div class="col-md-6">
                <div class="box box-danger box-solid">
                  <div class="box-header with-border">
                    <h3 class="box-title">Suami</h3>

                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                    </div>
                    <!-- /.box-tools -->
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                  <div class="col-md-12">
                    <div class="form-group">
                    <label>Nama:</label>
                    <div class="input-group">
                      
                      <!-- /btn-group -->
                      <input type="text" class="form-control" name="namas" id="namas" placeholder="Masukkan Nama Lengkap" readonly required>
                      <div class="input-group-btn">
                        <button type="button" data-toggle="modal" data-target="#modal-defaults" class="btn btn-info" >Cari</button>
                      </div>
                    </div>
                    </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Tempat Lahir:</label>
                        <input type="text" name="tempatlahirs" id="tempatlahirs" class="form-control" style="text-transform: uppercase" placeholder="Masukkan Tempat Lahir" readonly required>
                      </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group">
                      <label>Tanggal Lahir:</label>
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" name="tgllahirs" id="tgllahirs" class="form-control pull-right" id="datepicker2" placeholder="Masukkan Tanggal Lahir" readonly required>
                      </div>
                      <!-- /.input group -->
                    </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Agama:</label>
                        <input type="text" name="agamas" id="agamas" class="form-control" style="text-transform: uppercase" placeholder="Masukkan Agama" readonly required>
                      </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group">
                        <label>Pekerjaan:</label>
                        <input type="text" name="pekerjaans" id="pekerjaans" class="form-control" style="text-transform: capitalize" placeholder="Masukkan Pekerjaan" readonly required>
                      </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group">
                        <label>Dusun:</label>
                        <input type="text" name="dusuns" id="dusuns" class="form-control" style="text-transform: uppercase" placeholder="Masukkan Alamat Dusun" readonly required>
                      </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group">
                        <label>Desa:</label>
                        <input type="text" name="desas" id="kels" class="form-control" style="text-transform: uppercase" placeholder="Masukkan Alamat Desa" readonly required>
                      </div>
                    </div>
                    <div class="col-md-12">
                    <div class="form-group">
                        <label>Kecamatan:</label>
                        <input type="text" name="kecamatans" id="kecamatans" class="form-control" style="text-transform: capitalize" placeholder="Masukkan Alamat Kecamatan" readonly required>
                      </div>
                    </div>

                  </div>
                  <!-- /.box-body -->
                </div>
                <!-- /.box -->
              </div>
              <div class="col-md-6">
                <div class="box box-primary box-solid">
                  <div class="box-header with-border">
                    <h3 class="box-title">Istri</h3>

                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                    </div>
                    <!-- /.box-tools -->
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                  <div class="col-md-12">
                    <div class="form-group">
                    <label>Nama:</label>
                    <div class="input-group">
                      
                      <!-- /btn-group -->
                      <input type="text" class="form-control" name="namai" id="namai" placeholder="Masukkan Nama Lengkap" readonly required>
                      <div class="input-group-btn">
                        <button type="button" data-toggle="modal" data-target="#modal-defaulti" class="btn btn-info" >Cari</button>
                      </div>
                    </div>
                    </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Tempat Lahir:</label>
                        <input type="text" name="tempatlahiri" id="tempatlahiri" class="form-control" style="text-transform: uppercase" placeholder="Masukkan Tempat Lahir" readonly required>
                      </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group">
                      <label>Tanggal Lahir:</label>
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" name="tgllahiri" id="tgllahiri" class="form-control pull-right" id="datepicker" placeholder="Masukkan Tanggal Lahir" readonly required>
                      </div>
                      <!-- /.input group -->
                    </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Agama:</label>
                        <input type="text" name="agamai" id="agamai" class="form-control" style="text-transform: uppercase" placeholder="Masukkan Agama" readonly required>
                      </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group">
                        <label>Pekerjaan:</label>
                        <input type="text" name="pekerjaani" id="pekerjaani" class="form-control" style="text-transform: capitalize" placeholder="Masukkan Pekerjaan" readonly required>
                      </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group">
                        <label>Dusun:</label>
                        <input type="text" name="dusuni" id="dusuni" class="form-control" style="text-transform: uppercase" placeholder="Masukkan Alamat Dusun" readonly required>
                      </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group">
                        <label>Desa:</label>
                        <input type="text" name="desai" id="keli" class="form-control" style="text-transform: uppercase" placeholder="Masukkan Alamat Desa" readonly required>
                      </div>
                    </div>
                    <div class="col-md-12">
                    <div class="form-group">
                        <label>Kecamatan:</label>
                        <input type="text" name="kecamatani" id="kecamatani" class="form-control" style="text-transform: capitalize" placeholder="Masukkan Alamat Kecamatan" readonly required>
                      </div>
                    </div>
                  </div>
                  <!-- /.box-body -->
                </div>
                <!-- /.box -->
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Dicatat:</label>
                  <input type="text" name="oleh" class="form-control" style="text-transform: capitalize" placeholder="Dicatat dan Disaksikan Oleh" required>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Tanggal Nikah:</label>
                  <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" name="tglnikah" class="form-control pull-right" id="datepicker3" placeholder="Masukkan Tanggal Nikah" required>
                  </div>
                  <!-- /.input group -->
                </div>
                </div>
              </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" name="cetak" class="btn btn-success"><i class="fa fa-print"></i> Cetak</button>
                <a href="javascript:history.go(-1)" class="btn btn-default"><i class="fa fa-arrow-left"></i> Kembali</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- /.cari penduduk -->
<div class="modal fade " id="modal-defaults">
  <div class="modal-dialog  modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title modal-primary">Cari Data Penduduk</h4>
      </div>
      <div class="modal-body">
        <div class="box box-primary">
            <!-- /.box-header -->
            <div class="box-body">
            <table id="lookup" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>NIK</th>
                  <th>Nama</th>
                  <th>Tempat/Tgl Lahir</th>
                  <th>Jenis Kelamin</th>
                  <th>Dusun</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
<?php
  $no = 1;
  $sql = $koneksi->query("select * from penduduk where jk='Laki-Laki'");
  while ($data=$sql->fetch_assoc()){
?>
                <tr>
                  <td><?php echo $no++;?></td>
                  <td><?php echo $data['nik'];?></td>
                  <td><?php echo $data['nama'];?></td>
                  <td><?php echo $data['tempatlahir']?>/<?php echo date('d-m-Y', strtotime($data['tgllahir']));?></td>
                  <td><?php echo $data['jk'];?></td>
                  <td><?php echo $data['dusun'];?></td>
                  <td class="text-center pilih" data-nama="<?php echo $data['nama']; ?>" data-tempatlahir="<?php echo $data['tempatlahir']; ?>" data-tgllahir="<?php echo $data['tgllahir']; ?>" data-agama="<?php echo $data['agama']; ?>"
                  data-pekerjaan="<?php echo $data['pekerjaan']; ?>" data-dusun="<?php echo $data['dusun']; ?>"
                  data-kel="<?php echo $data['kel']; ?>" data-kecamatan="<?php echo $data['kecamatan']; ?>">
                    <button class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" title="Pilih"><i class="fa fa-check"></i></button>
                  </td>
                </tr>
<?php
  }
?>
                </tbody>
                <tfoot>
                <tr>
                <th>No</th>
                  <th>NIK</th>
                  <th>Nama</th>
                  <th>Tempat/Tgl Lahir</th>
                  <th>Jenis Kelamin</th>
                  <th>Dusun</th>
                  <th>Aksi</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->



  <!-- /.cari penduduk -->
  <div class="modal fade " id="modal-defaulti">
  <div class="modal-dialog  modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title modal-primary">Cari Data Penduduk</h4>
      </div>
      <div class="modal-body">
        <div class="box box-primary">
            <!-- /.box-header -->
            <div class="box-body">
            <table id="lookup2" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>NIK</th>
                  <th>Nama</th>
                  <th>Tempat/Tgl Lahir</th>
                  <th>Jenis Kelamin</th>
                  <th>Dusun</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
<?php
  $no = 1;
  $sql = $koneksi->query("select * from penduduk where jk='PEREMPUAN'");
  while ($data=$sql->fetch_assoc()){
?>
                <tr>
                  <td><?php echo $no++;?></td>
                  <td><?php echo $data['nik'];?></td>
                  <td><?php echo $data['nama'];?></td>
                  <td><?php echo $data['tempatlahir']?>/<?php echo date('d-m-Y', strtotime($data['tgllahir']));?></td>
                  <td><?php echo $data['jk'];?></td>
                  <td><?php echo $data['dusun'];?></td>
                  <td class="text-center pilihi" data-namai="<?php echo $data['nama']; ?>" data-tempatlahiri="<?php echo $data['tempatlahir']; ?>" data-tgllahiri="<?php echo $data['tgllahir']; ?>" data-agamai="<?php echo $data['agama']; ?>"
                  data-pekerjaani="<?php echo $data['pekerjaan']; ?>" data-dusuni="<?php echo $data['dusun']; ?>"
                  data-keli="<?php echo $data['kel']; ?>" data-kecamatani="<?php echo $data['kecamatan']; ?>">
                    <button class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" title="Pilih"><i class="fa fa-check"></i></button>
                  </td>
                </tr>
<?php
  }
?>
                </tbody>
                <tfoot>
                <tr>
                <th>No</th>
                  <th>NIK</th>
                  <th>Nama</th>
                  <th>Tempat/Tgl Lahir</th>
                  <th>Jenis Kelamin</th>
                  <th>Dusun</th>
                  <th>Aksi</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- /.cari penduduk -->
<!-- /.cari penduduk -->
<script>
  $(document).on('click', '.pilih', function (e) {
      document.getElementById("namas").value = $(this).attr('data-nama');
      document.getElementById("tempatlahirs").value = $(this).attr('data-tempatlahir');
      document.getElementById("tgllahirs").value = $(this).attr('data-tgllahir');
      document.getElementById("agamas").value = $(this).attr('data-agama');
      document.getElementById("pekerjaans").value = $(this).attr('data-pekerjaan');
      document.getElementById("dusuns").value = $(this).attr('data-dusun');
      document.getElementById("kels").value = $(this).attr('data-kel');
      document.getElementById("kecamatans").value = $(this).attr('data-kecamatan');
      $('#modal-defaults').modal('hide');
  });
            $(function () {
                $("#lookup").dataTable();
            });

  $(document).on('click', '.pilihi', function (e) {
      document.getElementById("namai").value = $(this).attr('data-namai');
      document.getElementById("tempatlahiri").value = $(this).attr('data-tempatlahiri');
      document.getElementById("tgllahiri").value = $(this).attr('data-tgllahiri');
      document.getElementById("agamai").value = $(this).attr('data-agamai');
      document.getElementById("pekerjaani").value = $(this).attr('data-pekerjaani');
      document.getElementById("dusuni").value = $(this).attr('data-dusuni');
      document.getElementById("keli").value = $(this).attr('data-keli');
      document.getElementById("kecamatani").value = $(this).attr('data-kecamatani');
      $('#modal-defaulti').modal('hide');
  });
            $(function () {
                $("#lookup2").dataTable();
            });

</script>
  