<?php
  $id = $_GET[id];
  $sql = $koneksi->query("select * from suratkeluar join nikah on suratkeluar.id_suratkeluar=nikah.id_suratkeluar
  where id_nikah='$id'") or mysqli_error($koneksi);
  $data = $sql->fetch_assoc();
  $agamas= $data['agamas'];
  $agamai = $data['agamai'];

    if (isset($_POST['simpan'])) {
      $oleh = ucwords($_POST['oleh']);
      $tglnikah = date('Y-m-d', strtotime($_POST['tglnikah']));

      $sql = $koneksi->query("update nikah set oleh='$oleh', tglnikah='$tglnikah' where id_nikah='$id'") or die(mysqli_error($koneksi));
      if ($sql==true) {
        ?>
        <script>
            swal({
                title: 'Suksess!',
                text: 'Data Berhasil Diedit',
                type: 'success',
                html: true,
                confirmButtonClass: 'btn-primary',
                confirmButtonText: 'Ooke',
                },function(){
                window.location.href = "?page=nikah"
                });
        </script>
        <?php 
      }
    }   
?>  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Nikah
        <small>Surat Keterangan</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="?page=nikah">Keterangan Nikah</a></li>
        <li class="active">Edit</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Data Surat Keterangan Nikah</h3>
            </div>
            <form role="form" method="POST">
              <div class="box-body">
              <div class="row">
              <div class="col-lg-12">
                <div class="text-center">
                    <label style="font-size:18pt;">Nomor Surat: <?php echo $data['no_suratkeluar']?></label>
                </div>
              </div>
              <br>
              <br>
              <br>
              <div class="col-md-6">
                <div class="box box-danger box-solid">
                  <div class="box-header with-border">
                    <h3 class="box-title">Suami</h3>

                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                    </div>
                    <!-- /.box-tools -->
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Nama Suami:</label>
                        <input type="text" name="namas" class="form-control" style="text-transform: capitalize" placeholder="Masukkan Nama Lengkap"
                        value="<?php echo $data['namas']?>" readonly required>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Tempat Lahir:</label>
                        <input type="text" name="tempatlahirs" class="form-control" style="text-transform: uppercase" placeholder="Masukkan Tempat Lahir" 
                        value="<?php echo $data['tempatlahirs']?>" readonly required>
                      </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group">
                      <label>Tanggal Lahir:</label>
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" name="tgllahirs" class="form-control pull-right" placeholder="Masukkan Tanggal Lahir" 
                        value="<?php echo date('d-m-Y' , strtotime($data['tgllahirs']))?>" readonly required>
                      </div>
                      <!-- /.input group -->
                    </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Agama:</label>
                        <input type="text" name="agamas" class="form-control" style="text-transform: capitalize" placeholder="Masukkan Agama" 
                        value="<?php echo $data['agamas']?>" readonly required>
                      </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group">
                        <label>Pekerjaan:</label>
                        <input type="text" name="pekerjaans" class="form-control" style="text-transform: capitalize" placeholder="Masukkan Pekerjaan" 
                        value="<?php echo $data['pekerjaans']?>" readonly required>
                      </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group">
                        <label>Dusun:</label>
                        <input type="text" name="dusuns" class="form-control" style="text-transform: uppercase" placeholder="Masukkan Alamat Dusun"
                        value="<?php echo $data['dusuns']?>" readonly required>
                      </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group">
                        <label>Desa:</label>
                        <input type="text" name="desas" class="form-control" style="text-transform: uppercase" placeholder="Masukkan Alamat Desa" 
                        value="<?php echo $data['desas']?>" readonly required>
                      </div>
                    </div>
                    <div class="col-md-12">
                    <div class="form-group">
                        <label>Kecamatan:</label>
                        <input type="text" name="kecs" class="form-control" style="text-transform: capitalize" placeholder="Masukkan Alamat Kecamatan" 
                        value="<?php echo $data['kecs']?>" readonly required>
                      </div>
                    </div>

                  </div>
                  <!-- /.box-body -->
                </div>
                <!-- /.box -->
              </div>
              <div class="col-md-6">
                <div class="box box-primary box-solid">
                  <div class="box-header with-border">
                    <h3 class="box-title">Istri</h3>

                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                    </div>
                    <!-- /.box-tools -->
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                  <div class="col-md-12">
                      <div class="form-group">
                        <label>Nama Istri:</label>
                        <input type="text" name="namai" class="form-control" style="text-transform: capitalize" placeholder="Masukkan Nama Lengkap" 
                        value="<?php echo $data['namai']?>" readonly required>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Tempat Lahir:</label>
                        <input type="text" name="tempatlahiri" class="form-control" style="text-transform: uppercase" placeholder="Masukkan Tempat Lahir"
                        value="<?php echo $data['tempatlahiri']?>" readonly required>
                      </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group">
                      <label>Tanggal Lahir:</label>
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" name="tgllahiri" class="form-control pull-right" placeholder="Masukkan Tanggal Lahir" 
                        value="<?php echo date('d-m-Y' , strtotime($data['tgllahiri']))?>" readonly required>
                      </div>
                      <!-- /.input group -->
                    </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Agama:</label>
                        <input type="text" name="agamai" class="form-control" style="text-transform: capitalize" placeholder="Masukkan Tempat Lahir"
                        value="<?php echo $data['agamai']?>" readonly required>
                      </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group">
                        <label>Pekerjaan:</label>
                        <input type="text" name="pekerjaani" class="form-control" style="text-transform: capitalize" placeholder="Masukkan Pekerjaan" 
                        value="<?php echo $data['pekerjaani']?>" readonly required>
                      </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group">
                        <label>Dusun:</label>
                        <input type="text" name="dusuni" class="form-control" style="text-transform: uppercase" placeholder="Masukkan Alamat Dusun" 
                        value="<?php echo $data['dusuni']?>" readonly required>
                      </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group">
                        <label>Desa:</label>
                        <input type="text" name="desai" class="form-control" style="text-transform: uppercase" placeholder="Masukkan Alamat Desa" 
                        value="<?php echo $data['desai']?>" readonly required>
                      </div>
                    </div>
                    <div class="col-md-12">
                    <div class="form-group">
                        <label>Kecamatan:</label>
                        <input type="text" name="keci" class="form-control" style="text-transform: capitalize" placeholder="Masukkan Alamat Kecamatan" 
                        value="<?php echo $data['keci']?>" readonly required>
                      </div>
                    </div>

                  </div>
                  <!-- /.box-body -->
                </div>
                <!-- /.box -->
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Dicatat:</label>
                  <input type="text" name="oleh" class="form-control" style="text-transform: capitalize" placeholder="Dicatat dan Disaksikan Oleh" 
                  value="<?php echo $data['oleh']?>" required>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Tanggal Nikah:</label>
                  <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" name="tglnikah" class="form-control pull-right" id="datepicker3" placeholder="Masukkan Tanggal Nikah"
                    value="<?php echo date('d-m-Y' , strtotime($data['tglnikah']))?>" required>
                  </div>
                  <!-- /.input group -->
                </div>
                </div>
              </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" name="simpan" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                <a href="javascript:history.go(-1)" class="btn btn-default"><i class="fa fa-arrow-left"></i> Kembali</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  