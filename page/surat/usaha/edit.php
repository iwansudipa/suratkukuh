<?php
  $id = $_GET[id];
  $sql = $koneksi->query("select * from suratkeluar join usaha on suratkeluar.id_suratkeluar=usaha.id_suratkeluar
  where id_usaha='$id'") or mysqli_error($koneksi);
  $data = $sql->fetch_assoc();

    if (isset($_POST['simpan'])) {
      $tahunusaha = $_POST['tahunusaha'];
      $pinjamanbank = ucwords($_POST['pinjamanbank']);
      $ket = strtoupper($_POST['ketusaha']);

      $sql = $koneksi->query("update usaha set tahunusaha='$tahunusaha', pinjamanbank='$pinjamanbank', ket='$ket' where id_usaha='$id' ") or die(mysqli_error($koneksi));
      if ($sql==true) {
        ?>
        <script>
            swal({
                title: 'Suksess!',
                text: 'Data Berhasil Diedit',
                type: 'success',
                html: true,
                confirmButtonClass: 'btn-primary',
                confirmButtonText: 'Ooke',
                },function(){
                window.location.href = "?page=usaha"
                });
        </script>
        <?php
      }
    }   
?>  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Usaha
        <small>Surat Keterangan</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="?page=usaha">Surat Keterangan</a></li>
        <li class="active">Edit</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Data Surat Keterangan Usaha</h3>
            </div>
            <form role="form" method="POST">
              <div class="box-body">
              <div class="row">
              <div class="col-lg-12">
                <div class="text-center">
                    <label style="font-size:18pt;">Nomor Surat: <?php echo $data['no_suratkeluar']?></label>
                </div>
              </div>
              <br>
              <br>
              <br>
              <div class="col-md-6">
              <div class="form-group">
                  <label>NIK:</label>
                  <input type="text" name="nik" class="form-control" style="text-transform: uppercase" placeholder="Masukkan NIK" 
                  value="<?php echo $data['nik']?>" readonly required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Nama:</label>
                  <input type="text" name="nama" class="form-control" style="text-transform: uppercase" placeholder="Masukkan Nama" 
                  value="<?php echo $data['nama']?>" readonly required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Jenis Kelamin:</label>
                  <input type="text" name="jk" id="jk" class="form-control" style="text-transform: capitalize" value="<?php echo $data['jk']?>" placeholder="Masukkan Jenis Kelamin" readonly required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Tempat Lahir:</label>
                  <input type="text" name="tempatlahir" class="form-control" style="text-transform: capitalize" placeholder="Masukkan Tempat Lahir" 
                  value="<?php echo $data['tempatlahir']?>" readonly required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                <label>Tanggal Lahir:</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" name="tgllahir" class="form-control pull-right" id="tgllahir" value="<?php echo date('d-m-Y', strtotime($data['tgllahir']))?>" placeholder="Masukkan Tanggal Lahir" readonly required>
                </div>
                <!-- /.input group -->
              </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Agama:</label>
                  <input type="text" name="agama" id="agama" value="<?php echo $data['agama']?>" class="form-control" style="text-transform: uppercase" placeholder="Masukkan Agama" readonly required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Status Kawin:</label>
                  <input type="text" name="statuskawin" id="statuskawin" value="<?php echo $data['statuskawin']?>" class="form-control" style="text-transform: uppercase" placeholder="Masukkan Status Kawin" readonly required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Pekerjaan:</label>
                  <input type="text" name="pekerjaan" class="form-control" style="text-transform: capitalize" placeholder="Masukkan Pekerjaan" 
                  value="<?php echo $data['pekerjaan']?>" readonly required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Dusun:</label>
                  <input type="text" name="dusun" id="dusun" value="<?php echo $data['dusun']?>" class="form-control" style="text-transform: capitalize" placeholder="Masukkan Dusun" readonly required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>RT/RW:</label>
                  <input type="text" name="rt" id="rt" value="<?php echo $data['rt']?>" class="form-control" style="text-transform: capitalize" placeholder="Masukkan RT/RW" readonly required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Kel/Desa:</label>
                  <input type="text" name="kel" id="kel" value="<?php echo $data['kel']?>" class="form-control" style="text-transform: capitalize" placeholder="Masukkan Kel/Desa" readonly required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Kecamatan:</label>
                  <input type="text" name="kecamatan" id="kecamatan" value="<?php echo $data['kecamatan']?>" class="form-control" style="text-transform: capitalize" placeholder="Masukkan Kecamatan" readonly required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Tahun Usaha:</label>
                  <input type="text" name="tahunusaha" class="form-control" placeholder="Masukkan Tahun" pattern="[0-9]+" title="Hanya tahun" 
                  value="<?php echo $data['tahunusaha']?>" required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Pinjaman Bank:</label>
                  <input type="text" name="pinjamanbank" class="form-control" style="text-transform: capitalize" placeholder="Masukkan Nama BANK" 
                  value="<?php echo $data['pinjamanbank']?>" required>
                </div>
              </div>
              <div class="col-md-12">
              <div class="form-group">
                  <label>Keterangan Usaha:</label>
                  <textarea name="ketusaha" class="form-control" rows="4" style="text-transform: uppercase" placeholder="Masukkan Keterangan Usaha" required><?php echo $data['ket']?></textarea>
              </div>
              </div>

              </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" name="simpan" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                <a href="javascript:history.go(-1)" class="btn btn-default"><i class="fa fa-arrow-left"></i> Kembali</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  