<?php
    if (isset($_POST['simpan'])) {
      $nosurat=$_POST['nosurat'];
      $asalsurat=$_POST['asalsurat'];
      $tglsurat=date('Y-m-d', strtotime($_POST['tglsurat']));
      $tglterima=date('Y-m-d', strtotime($_POST['tglterima']));
      $perihal=$_POST['perihal'];
      $sifatsurat=$_POST['sifatsurat'];
      $statusdisposisi = 'Belum';
      $error = $_FILES['file']['error'];

      $sql2=$koneksi->query("select * from suratmasuk where no_suratmasuk='$nosurat'");
      $cek=$sql2->num_rows;
        if ($cek>0) {
          ?>
          <script>
            swal("Gagal!", "Nomor Surat Sudah ada", "error");
          </script>
          <?php
        }else{
      if ($error==0) {
        $extensi = explode(".", $_FILES['file']['name']);
        $nama_file = "doc-".round(microtime(true)).".".end($extensi);
        $format = pathinfo($nama_file, PATHINFO_EXTENSION);
            if ($format=="pdf") {
                $file_asal = $_FILES['file']['tmp_name'];
                $upload = move_uploaded_file($file_asal, 'doc/'.$nama_file);
                if ($upload==true) {
                  $sql = $koneksi->query("insert into suratmasuk (no_suratmasuk, tgl_suratmasuk, tgl_terima, sifat, asal, perihal, status_disposisi, file)
                  values('$nosurat','$tglsurat','$tglterima','$sifatsurat','$asalsurat','$perihal','$statusdisposisi','$nama_file')") or die(mysqli_error($koneksi));
                  ?>
                    <script>
                      swal({
                            title: 'Suksess!',
                            text: 'Data Berhasil Ditambah',
                            type: 'success',
                            html: true,
                            confirmButtonClass: 'btn-primary',
                            confirmButtonText: 'Ooke',
                            },function(){
                            window.location.href = "?page=suratmasuk"
                          });
                    </script>
                  <?php
                }else {
                    echo "Upload Gagal";
                }
            }else {
              ?>
              <script>
                swal("Gagal!", "Format file harus PDF (.pdf)!", "error");
              </script>
              <?php
            }
    }else{
      ?>
      <script>
        swal("Gagal!", "Format file harus PDF (.pdf)!", "error");
      </script>
      <?php
    }   
    }
  }
?>  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Surat Masuk
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="?page=suratmasuk">Surat Masuk</a></li>
        <li class="active">Tambah</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah Data Surat Masuk</h3>
            </div>
            <form role="form" method="POST" enctype="multipart/form-data">
              <div class="box-body">
              <div class="row">
                <div class="col-lg-12" >
              <div class="alert alert-warning alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-warning"></i> Perhatian!</h4>
                Silahkan isi form dibawah ini untuk menambah data surat masuk
              </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>No Surat:</label>
                  <input type="text" name="nosurat" class="form-control" placeholder="Masukkan No Surat" 
                  value="<?php if (isset($_POST['simpan'])) {
                    echo $nosurat;
                  }?>" required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                <label>Tanggal Surat Masuk:</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" name="tglsurat" class="form-control pull-right" id="datepicker" placeholder="Masukkan Tanggal" 
                  value="<?php if (isset($_POST['simpan'])) {
                    echo date('d-m-Y', strtotime($tglsurat));
                  }?>" required>
                </div>
                <!-- /.input group -->
              </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Asal Surat:</label>
                  <input type="text" name="asalsurat" class="form-control" placeholder="Masukkan Asal Surat" 
                  value="<?php if (isset($_POST['simpan'])) {
                    echo $asalsurat;
                  }?>" required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                <label>Sifat Surat:</label>
                <select class="form-control select2" style="width: 100%;" name="sifatsurat" required>
                  <option selected="disable selected" value="">=>Pilih Sifat Surat<=</option>
                  <option value="Biasa" <?php if (isset($_POST['simpan'])) {
                    if ($sifatsurat=="Biasa") {
                      echo "selected";
                    };
                  }?>>Biasa</option>
                  <option value="Penting" <?php if (isset($_POST['simpan'])) {
                    if ($sifatsurat=="Penting") {
                      echo "selected";
                    };
                  }?>>Penting</option>
                  <option value="Sangat Penting" <?php if (isset($_POST['simpan'])) {
                    if ($sifatsurat=="Sangat Penting") {
                      echo "selected";
                    };
                  }?>>Sangat Penting</option>
                  <option value="Rahasia" <?php if (isset($_POST['simpan'])) {
                    if ($sifatsurat=="Rahasia") {
                      echo "selected";
                    };
                  }?>>Rahasia</option>
                </select>
              </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Perihal:</label>
                  <textarea name="perihal" class="form-control" rows="5" placeholder="Masukkan Perihal" required><?php if (isset($_POST['simpan'])){echo $perihal;}?></textarea>
              </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                <label>Tanggal Terima:</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" name="tglterima" class="form-control pull-right" id="datepicker2" 
                  value="<?php if (isset($_POST['simpan'])) {
                    echo date('d-m-Y', strtotime($tglterima));
                  }?>" placeholder="Masukkan Tanggal" required>
                </div>
                <!-- /.input group -->
              </div>
              </div>
              <div class="col-lg-6">
              <div class="form-group">
                  <label for="exampleInputFile">Masukkan File</label>
                  <input type="file" id="exampleInputFile" name="file" accept="application/pdf">
                  <p class="help-block">Format file harus PDF (.pdf).</p>
                </div>
              </div>
              
              
              
              </div>

              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" name="simpan" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                <a href="javascript:history.go(-1)" class="btn btn-default"><i class="fa fa-arrow-left"></i> Kembali</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  