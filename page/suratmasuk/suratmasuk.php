  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Surat Masuk
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Surat Masuk</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">Data Surat Masuk</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped table-responsive">
                <thead>
                <tr>
                  <th>No</th>
                  <th>No Surat</th>
                  <th width="70px">Tgl. Surat</th>
                  <th width="80px">Tgl. Terima</th>
                  <th>Asal</th>
                  <th>Sifat</th>
                  <th>Perihal</th>
                  <th>Status Disposisi</th>
                  <th width="30px">Aksi</th>
                </tr>
                </thead>
                <tbody>
<?php
  $no = 1;
  $sql = $koneksi->query("select * from suratmasuk");
  while ($data=$sql->fetch_assoc()){
?>
                <tr>
                  <td><?php echo $no++;?></td>
                  <td><?php echo $data['no_suratmasuk'];?></td>
                  <td><?php echo date('d-m-Y', strtotime($data['tgl_suratmasuk']));?></td>
                  <td><?php echo date('d-m-Y', strtotime($data['tgl_terima']));?></td>
                  <td><?php echo $data['asal'];?></td>
                  <td><?php echo $data['sifat'];?></td>
                  <td><?php echo $data['perihal'];?></td>
                  <td class="text-center"><i class="<?php if ($data['status_disposisi']=='Sudah') {
                    echo"fa fa-check label label-success";
                  }else {
                    echo"fa fa-close label label-danger";
                  }?>">
                  <?php echo $data['status_disposisi'];?></i> </td>
                  <td class="text-center">
                    <a href="?page=suratmasuk&aksi=edit&id_suratmasuk=<?php echo $data['id_suratmasuk'];?>" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></a>
                    <a href="?page=suratmasuk&aksi=hapus&id_suratmasuk=<?php echo $data['id_suratmasuk'];?>" class="btn btn-danger btn-xs hapus" data-toggle="tooltip" data-placement="top" title="Hapus"><i class="fa fa-trash"></i></a>
                    <!-- <a href="?page=suratmasuk&aksi=detail&id_suratmasuk=<?php echo $data['id_suratmasuk'];?>" data-toggle="tooltip" data-placement="top" title="Detail" class="btn btn-warning btn-xs" ><i class="fa fa-pencil-square"></i></a> -->
                  </td>
                </tr>
<?php
  }
?>
                </tbody>
                <tfoot>
                <tr>
                  <th>No</th>
                  <th>No Surat</th>
                  <th>Tgl. Surat</th>
                  <th>Tgl. Terima</th>
                  <th>Asal</th>
                  <th>Sifat</th>
                  <th>Perihal</th>
                  <th>Status Disposisi</th>
                  <th>Aksi</th>
                </tr>
                </tfoot>
              </table>
              <a href="?page=suratmasuk&aksi=tambah" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah</a>
              <a href="javascript:history.go(-1)" class="btn btn-default"><i class="fa fa-arrow-left"></i> Kembali</a>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<script>
  $('.hapus').on('click',function(){
      var getLink = $(this).attr('href');
        swal({
              title: 'Hapus',
              text: 'Apakah anda yakin untuk menghapus data?',
              type: "warning",
              html: true,
              showCancelButton: true,
              confirmButtonClass: 'btn-danger',
              confirmButtonText: 'Hapus!',
              cancelButtonText: "Batal!",
              },function(){
              window.location.href = getLink
          });
      return false;
        });
</script>