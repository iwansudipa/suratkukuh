<?php
  $id = $_GET[id_suratmasuk];
  $sql = $koneksi->query("select * from suratmasuk where id_suratmasuk='$id'");
  $data = $sql->fetch_assoc();
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Surat Masuk
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="?page=suratmasuk">Surat Masuk</a></li>
        <li class="active">Detail</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
    <div class="row">
    <div class="col-md-12">
    <div class="box box-danger">
          <div class="box box-solid">
            <div class="box-header with-border">

              <h3 class="box-title">Detail Data Surat Masuk</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            

              <div class="col-lg-5">
              <dl class="dl-horizontal">
                <dt>No Surat:</dt>
                <dd><?php echo $data['no_suratmasuk'];?></dd>
                <dt>Tanggal Surat Masuk:</dt>
                <dd><?php echo date('d-m-Y', strtotime($data['tgl_suratmasuk']));?></dd>
                <dt>Tanggal Terima:</dt>
                <dd><?php echo date('d-m-Y', strtotime($data['tgl_terima']));?></dd>
                <dt>Sifat:</dt>
                <dd><?php echo $data['sifat']?></dd>
                <dt>Asal Surat Masuk:</dt>
                <dd><?php echo $data['asal']?></dd>
                <dt>Perihal:</dt>
                <dd><?php echo $data['perihal']?></dd>
                <dt>Status:</dt>
                <dd><?php echo $data['status_disposisi']?></dd>
                <dt>Document:</dt>
                <dd><a href="doc/<?php echo $data['file']?>" data-toggle="tooltip" data-placement="right" title="Lihat Dokument">
                <img src="img/pdf.png" height="100" ></a></dd>
              </dl>
              <div class="box-footer">
              <?php 
                if ($data['status_disposisi']=='Belum') {
                  echo "<button data-toggle='modal' data-target='#modal-default' class='btn btn-success'><i class='fa fa-book'></i> Disposisi</button>";
                }
              ?>
                <a href="javascript:history.go(-1)" class="btn btn-default"><i class="fa fa-arrow-left"></i> Kembali</a>
          </div>
          </div>
          <div class="col-lg-7">
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-ban"></i> Keterangan!</h4>
                Silahkan periksa terlebih dahulu data surat masuk sebelum didisposisikan.
              </div>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          </div>
        </div>
        <!-- ./col -->
        </div>
    <!-- /.content -->
<!-- /.disposisi -->
        <div class="modal fade" id="modal-default">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Disposisi Surat Masuk</h4>
              </div>
              <div class="modal-body">
                <form role="form" action="" method="POST">
                <div class="form-group">
                  <label>No. Surat Masuk:</label>
                  <input type="text" name="nosurat" class="form-control"value="<?php echo $data['no_suratmasuk']; ?>" readonly>
                </div>
                <div class="form-group">
                  <label>Diteruskan:</label>
                  <input type="text" name="diteruskan" class="form-control" placeholder="Diteruskan" required>
                </div>
                <div class="form-group">
                  <label>Intruksi/Informasi:</label>
                  <textarea name="informasi" class="form-control" rows="3" placeholder="Masukkan Catatan"></textarea>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Batal</button>
                <button type="submit" name="disposisi" class="btn btn-success"><i class="fa fa-book"></i> Disposisi</button>
              </div>
              </form>
<?php
  if (isset($_POST['disposisi'])) {
    $nosurat = $_POST['nosurat'];
    $kode = $_POST['kode'];
    $diteruskan = $_POST['diteruskan'];
    $informasi = $_POST['informasi'];
    $disposisi = 'Sudah';

    $sql=$koneksi->query("insert into disposisi (id_suratmasuk, kode, diteruskan, informasi) values ('$id','$kode', '$diteruskan','$informasi')");
    $sql2=$koneksi->query("update suratmasuk set status_disposisi='$disposisi' where id_suratmasuk='$id'");
    ?>
      <script>
        swal({
              title: 'Suksess!',
              text: 'Data berhasil disposisikan',
              type: 'success',
              html: true,
              confirmButtonClass: 'btn-primary',
              confirmButtonText: 'Ooke',
              },function(){
              window.location.href = "?page=suratmasuk"
            });
      </script>
    <?php
  }
?>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
<!-- /.disposisi -->
    </section>
  </div>
  <!-- /.content-wrapper -->