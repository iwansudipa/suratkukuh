  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Pengguna
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Pengguna</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">Data Tabel Pengguna</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped table-responsive">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>Tempat Lahir</th>
                  <th>Tanggal Lahir</th>     
                  <th>Email</th>
                  <th>Jenis Kelamin</th>
                  <th>Level</th>
                  <th>Foto</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
<?php
  $no = 1;
  $sql = $koneksi->query("select * from user");
  while ($data=$sql->fetch_assoc()){
?>
                <tr>
                  <td><?php echo $no++;?></td>
                  <td><?php echo $data['nama'];?></td>
                  <td><?php echo $data['tempatlahir'];?></td>
                  <td><?php echo date('d-m-Y', strtotime($data['tanggallahir']));?></td>
                  <td><?php echo $data['email'];?></td>
                  <td><?php echo $data['jk'];?></td>
                  <td><?php echo $data['level'];?></td>
<?php if ($data['foto']=="avatar3.png" or $data['foto']=="avatar5.png") {
?>
                  <td><img src="img/user/<?php echo $data['foto'];?>" alt="" width="50" height="50"></td>
<?php }else{
?>
                  <td><img src="img/<?php echo $data['foto'];?>" alt="" width="50" height="50"></td>
<?php }?>
                  
                  <td class="text-center">
                    <a href="?page=pengguna&aksi=edit&id=<?php echo $data['id_user'];?>" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></a>
                    <a href="?page=pengguna&aksi=hapus&id=<?php echo $data['id_user'];?>" class="btn btn-danger btn-xs hapus" data-toggle="tooltip" data-placement="top" title="Hapus"><i class="fa fa-trash"></i></a>
                  </td>
                </tr>
<?php
  }
?>
                </tbody>
                <tfoot>
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>Tempat Lahir</th>
                  <th>Tanggal Lahir</th>          
                  <th>Email</th>
                  <th>Jenis Kelamin</th>
                  <th>Level</th>
                  <th>Foto</th>
                  <th>Aksi</th>
                </tr>
                </tfoot>
              </table>
              <a href="?page=pengguna&aksi=tambah" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah</a>
              <a href="javascript:history.go(-1)" class="btn btn-default"><i class="fa fa-arrow-left"></i> Kembali</a>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<script>
  $('.hapus').on('click',function(){
      var getLink = $(this).attr('href');
        swal({
              title: 'Hapus',
              text: 'Apakah anda yakin untuk menghapus data?',
              type: "warning",
              html: true,
              showCancelButton: true,
              confirmButtonClass: 'btn-danger',
              confirmButtonText: 'Hapus!',
              cancelButtonText: "Batal!",
              },function(){
              window.location.href = getLink
          });
      return false;
        });
</script>
