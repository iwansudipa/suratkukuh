<?php
    $id = $_GET[id];
    $sql=$koneksi->query("select * from user where id_user='$id'");
    $data=$sql->fetch_assoc();
    $level=$data['level'];
    $jk = $data['jk'];
 ?> 
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Pengguna
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="?page=pengguna">Pengguna</a></li>
        <li class="active">Edit</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Data Pengguna</h3>
            </div>
            <form role="form" method="POST" enctype="multipart/form-data">
              <div class="box-body">
              <div class="row">
              <div class="col-md-12">
              <div class="alert alert-warning alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-warning"></i> Perhatian!</h4>
                Silahkan ganti isi form dibawah ini untuk mengubah data pengguna yang salah
              </div>
                <div class="form-group">
                  <label>Nama Lengkap:</label>
                  <input type="text" name="nama" class="form-control" placeholder="Masukkan Nama Lengkap" 
                  value="<?php echo $data['nama']?>" required>
                </div>
              </div>   
            <div class="col-md-6">
                <div class="form-group">
                  <label for="exampleInputEmail1">Email</label>
                  <input type="email" class="form-control" name="email" id="exampleInputEmail1" placeholder="Masukkan email"
                  value="<?php echo $data['email'] ?>">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="exampleInputPassword1">Password</label>
                  <input type="password" class="form-control" name="password" id="exampleInputPassword1" placeholder="Password" 
                  value="<?php echo $data['password']?>" required>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Tempat Lahir:</label>
                  <input type="text" name="tempatlahir" class="form-control" placeholder="Masukkan Tempat Lahir" 
                  value="<?php echo $data['tempatlahir']?>" required>
                </div>
              </div>   
              <div class="col-md-6">
              <div class="form-group">
                <label>Tanggal Lahir:</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" name="tgllahir" class="form-control pull-right" id="datepicker" placeholder="Masukkan Tanggal" 
                  value="<?php echo date('d-m-Y', strtotime($data['tanggallahir']))?>" required>
                </div>
                <!-- /.input group -->
              </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                <label>Jenis Kelamin:</label>
                <select class="form-control select2" style="width: 100%;" name="jk" required>
                  <option selected="disable selected" value="">=>Jenis Kelamin<=</option>
                  <option value="laki-laki" <?php if ($jk=="laki-laki") {
                    echo "selected";
                  }?>>Laki-laki</option>
                  <option value="perempuan" <?php if ($jk=="perempuan") {
                    echo "selected";
                  }?>>Perempuan</option>
                </select>
              </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                <label>Level:</label>
                <select class="form-control select2" style="width: 100%;" name="level" required>
                  <option selected="disable selected" value="">=>Pilih Level<=</option>
                  <option value="Admin" <?php if ($level=="Admin") {
                      echo "selected";
                  }?>>Admin</option>
                  <option value="Kepala Desa" <?php if ($level=="Kepala Desa") {
                      echo "selected";
                  }?>>Kepala Desa</option>
                </select>
              </div>
              </div>
              </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" name="simpan" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                <a href="javascript:history.go(-1)" class="btn btn-default"><i class="fa fa-arrow-left"></i> Kembali</a>
              </div>
            </form>
            
<?php
    if (isset($_POST['simpan'])) {
      $nama=$_POST['nama'];
      $tgllahir= date('Y-m-d', strtotime($_POST['tgllahir']));
      $email=$_POST['email'];
      $password=$_POST['password'];
      $jk = $_POST['jk'];
      $level=$_POST['level'];
      $tempatlahir = $_POST['tempatlahir'];

      $sql=$koneksi->query("update user set nama='$nama', tanggallahir='$tgllahir', email='$email', password='$password',jk='$jk', level='$level', tempatlahir='$tempatlahir' where id_user='$id'");
      if ($sql==true) {
        ?>
        <script>
          swal({
                title: 'Suksess!',
                text: 'Data Berhasil Diedit',
                type: 'success',
                html: true,
                confirmButtonClass: 'btn-primary',
                confirmButtonText: 'Ooke',
                },function(){
                window.location.href = "?page=pengguna"
              });
        </script>
        <?php
       }else {
        ?>
        <script>
          swal("Gagal!", "Terjadi kesalahan!", "error");
        </script>
        <?php
       }   
    }
?>  
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  