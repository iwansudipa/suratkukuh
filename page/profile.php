<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Profile
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Profile</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
<?php 
    if ($data['foto']=="avatar5.png" or $data['foto']=="avatar3.png") {
?>
                <img class="profile-user-img img-responsive img-circle" src="img/user/<?php echo $data['foto']?>" alt="User profile picture">
<?php
  }else {
?>
               <img class="profile-user-img img-responsive img-circle" src="img/<?php echo $data['foto']?>" alt="User profile picture">
<?php
  }
?>
              

              <h3 class="profile-username text-center"><?php echo $data['nama']?></h3>

              <p class="text-muted text-center">Login Sebagai <?php echo $data['level']?></p>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                <b>Email</b> <a class="pull-right"><?php echo $data['email']?></a>
                </li>
              </ul>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          
          <!-- About Me Box -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Tentang Saya</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <strong><i class="fa fa-user margin-r-5"></i> Tempat dan tgl. lahir</strong>
<?php
  $umur = date('Y')-date('Y', strtotime($data['tanggallahir']));
?>
              <p class="text-muted"><?php echo $data['tempatlahir']?>, <?php echo date('d-m-Y', strtotime($data['tanggallahir']))?><br>
              Jenis Kelamin: <?php echo ucwords($data['jk'])?><br>
              Umur: <?php echo $umur?> tahun
              </p>


              <hr>

              <strong><i class="fa fa-map-marker margin-r-5"></i> Alamat</strong>

              <p class="text-muted"><?php echo $data['alamat']?></p>

              <hr>

              <strong><i class="fa fa-file-text-o margin-r-5"></i> Catatan</strong>

              <p><?php echo $data['catatan']?></p>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#activity" data-toggle="tab">Detail User</a></li>
              <li><a href="#settings" data-toggle="tab">Ubah Password</a></li>
            </ul>
            <div class="tab-content">
              <div class="active tab-pane" id="activity">
              <div class="post">
<?php
    $alamat = $data['alamat'];
    $catatan = $data['catatan'];
    if (empty($alamat and $catatan)) {
?>
              <div class="alert alert-warning alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-warning"></i> Perhatian!</h4>
                Silahkan lengkapi data profile anda.
              </div>
<?php }?>
              <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data">
              <div class="box-body">
              <div class="row margin-bottom">
              <div class="col-sm-4 ">
              <div class="form-group">
              <ul class="img-thumbnail text-center">
<?php 
    if ($data['foto']=="avatar5.png" or $data['foto']=="avatar3.png") {
?>
               <img src="img/user/<?php echo $data['foto']?>" width="215" height="215" alt="avatar">
<?php
  }else {
?>
               <img src="img/<?php echo $data['foto']?>" width="215" height="215" alt="avatar">
<?php
  }
?>
                  <label class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-camera"></i> Ubah Foto
                        <input type="file" id="upload_image" class="sr-only" name="file" accept="image/jpeg, image/png">
                  </label>
              </ul>
              <div class="callout callout-danger">
                <h4><i class="fa fa-ban"></i> Keterangan!</h4>

                <p style="font-size:9pt">Format foto harus Jpg(.jpg) dan Png(.png).</p>
              </div>
                </div>
                </div>                          
              <div class="col-sm-8 fa-pull-right">
              <div class="row">
              <div class="form-group">
                  <label class="col-sm-3 control-label">Nama Lengkap</label>

                  <div class="col-sm-9">
                    <input type="text" class="form-control" name="nama" placeholder="Masukkan Nama Lengkap"
                    value="<?php echo $data['nama']?>" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Email</label>

                  <div class="col-sm-9">
                    <input type="email"  name="email" class="form-control" id="inputEmail3" placeholder="Email" 
                    value="<?php echo $data['email']?>" required>
                  </div>
                </div>
                <div class="form-group">
                <label class="col-sm-3 control-label">Tanggal Lahir</label>
                <div class="col-sm-9">
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  
                  <input type="text" name="tgllahir" class="form-control pull-right" id="datepicker" placeholder="Masukkan Tanggal" 
                  value="<?php echo date('d-m-Y', strtotime($data['tanggallahir']))?>" required>
                </div>
                </div>
                <!-- /.input group -->
              </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">Tempat Lahir</label>

                  <div class="col-sm-9">
                    <input type="text" name="tempatlahir" class="form-control" id="inputPassword3" placeholder="Masukkan Tempat Lahir" 
                    value="<?php echo $data['tempatlahir']?>" required>
                  </div>
                </div>
               
<?php
  $jk = $data['jk'];
?>
              <div class="form-group">
                <label  class="col-sm-3 control-label">Jenis Kelamin</label>
                <div class="col-sm-9">
                <select class="form-control select2" style="width: 100%;" name="jk" required>
                  <option selected="disable selected" value="">=>Jenis Kelamin<=</option>
                  <option value="laki-laki" <?php if ($jk=="laki-laki") {
                      echo "selected";
                  }?>>Laki-Laki</option>
                  <option value="perempuan" <?php if ($jk=="perempuan") {
                      echo "selected";
                  }?>>Perempuan</option>
                </select>
              </div>
              </div>
              <div class="form-group">
                  <label class="col-sm-3 control-label">Alamat</label>

                  <div class="col-sm-9">
                  <textarea name="alamat" class="form-control" rows="3" placeholder="Masukkan Alamat" required><?php echo $data['alamat']?></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">Catatan</label>

                  <div class="col-sm-9">
                  <textarea name="catatan" class="form-control" rows="3" placeholder="Masukkan Catatan" required><?php echo $data['catatan']?></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-success" name="simpan"><i class="fa fa-save"></i> Simpan</button>
                  </div>
                </div>
              </div>
              </div>
              </div>
              </div>
              <!-- /.box-body -->
              
              <!-- /.box-footer -->
            </form>
<!-- Modal -->
    <div class="modal modal-primary" id="uploadimageModal">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Crop dan Simpan Foto</h4>
              </div>
              <div class="modal-body">
              <div class="alert alert-warning alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-warning"></i> Perhatian!</h4>
                Silahkan tentukan area foto yang dicrop!
              </div>
              <div class="row">
              <div class="ccol-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 text-center">
                <div id="image_demo"></div>
                <button class="btn btn-success crop_image"><i class="fa fa-crop"></i> Crop & Simpan</button>
              </div>
            </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-right" data-dismiss="modal">Close</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
<?php
    if (isset($_POST['simpan'])) {
      $id=$data['id_user'];
      $nama=$_POST['nama'];
      $email=$_POST['email'];
      $tempatlahir=$_POST['tempatlahir'];
      $tgllahir=date('Y-m-d', strtotime($_POST['tgllahir']));
      $jk =$_POST['jk'];
      $alamat = $_POST['alamat'];
      $catatan = $_POST['catatan'];

      $sql=$koneksi->query("update user set nama='$nama', email='$email', tempatlahir='$tempatlahir', tanggallahir='$tgllahir', jk='$jk', alamat='$alamat', catatan='$catatan' where id_user='$id'")
      or die(mysqli_error($koneksi));
      if ($sql==true) {
        ?>
        <script>
            swal({
                title: 'Suksess!',
                text: 'Data Berhasil disimpan',
                type: 'success',
                html: true,
                confirmButtonClass: 'btn-primary',
                confirmButtonText: 'Oke',
                },function(){
                window.location.href = "?page=profile"
                });
        </script>
        <?php
      }
    }
?>  
            </div>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="settings">
                <form class="form-horizontal" method="POST" action="" >
                  <div class="form-group">
                    <label class="col-sm-2 control-label">Password Lama</label>

                    <div class="col-sm-10">
                      <input type="password" class="form-control" name="passwordlama" placeholder="Password lama">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label">Password Baru</label>

                    <div class="col-sm-10">
                      <input type="password" class="form-control" name="passwordbaru" placeholder="Password baru">
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button type="submit" class="btn btn-success" name="ubah"><i class="fa fa-save"></i> Ubah</button>
                    </div>
                  </div>
                </form>
<?php 
    if (isset($_POST['ubah'])) {
      $id = $data['id_user'];
      $lama = $_POST['passwordlama'];
      $baru = $_POST['passwordbaru'];

      $sql = $koneksi->query("select * from user where id_user='$id' and password='$lama'") or die(mysqli_error($koneksi));
      $cek = $sql->num_rows;
      if ($cek==0) {
        ?>
        <script>
            swal("Gagal!", "Password lama salah", "error");
        </script>
        <?php
      }else {
        $sql= $koneksi->query("update user set password='$baru' where id_user='$id' and password='$lama'") or die(mysqli_error($koneksi));
         ?>
         <script>
             swal({
                 title: 'Suksess!',
                 text: 'Ubah password berhasil',
                 type: 'success',
                 html: true,
                 confirmButtonClass: 'btn-primary',
                 confirmButtonText: 'Ooke',
                 },function(){
                 window.location.href = "?page=profile"
                 });
         </script>
         <?php
      }
    }
?>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>

<script>  
$(document).ready(function(){

	$image_crop = $('#image_demo').croppie({
    enableExif: true,
    viewport: {
      width:215,
      height:215,
      type:'square' //circle
    },
    boundary:{
      width:300,
      height:300
    }
  });

  $('#upload_image').on('change', function(){
    var reader = new FileReader();
    reader.onload = function (event) {
      $image_crop.croppie('bind', {
        url: event.target.result
      }).then(function(){
        console.log('jQuery bind complete');
      });
    }
    reader.readAsDataURL(this.files[0]);
    $('#uploadimageModal').modal('show');
  });

  $('.crop_image').click(function(event){
    $image_crop.croppie('result', {
      type: 'canvas',
      size: 'viewport'
    }).then(function(response){
      $.ajax({
        url:"config/upload.php",
        type: "POST",
        data:{"image": response},
        success:function(data)
        {
          $('#uploadimageModal').modal('hide');
          swal({
                    title: 'Suksess!',
                    text: data,
                    type: 'success',
                    html: true,
                    confirmButtonClass: 'btn-primary',
                    confirmButtonText: 'Ooke',
                    },function(){
                    window.location.href = "?page=profile"
                    });
        }
      });
    })
  });

});  
</script>