 <?php
 $sql=$koneksi->query("select count(*) as jumlah from suratmasuk where tgl_suratmasuk between date_add(date(now()), interval -7 day) and date(now())");
 $data=$sql->fetch_assoc();
 $jsuratmasuk=$data['jumlah'];

 $sql2=$koneksi->query("select count(*) as jumlah from suratkeluar where tgl_suratkeluar between date_add(date(now()), interval -7 day) and date(now())");
 $data2=$sql2->fetch_assoc();
 $jsuratkeluar=$data2['jumlah'];

 $sql3=$koneksi->query("select count(*) as jumlah from suratmasuk join disposisi on suratmasuk.id_suratmasuk=disposisi.id_suratmasuk");
 $data3=$sql3->fetch_assoc();
 $jdisposisi=$data3['jumlah'];

 $sql4=$koneksi->query("select count(*) as jumlah from user");
 $data4=$sql4->fetch_assoc();
 $jpengguna=$data4['jumlah'];

  $sql=$koneksi->query("select * from user where id_user='$admin'");
  $data = $sql->fetch_assoc();
 ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard 
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
    <div class="row">
    <div class="col-md-12">
        <div class="callout callout-success">
                <h4>Hayy! <?php echo $data['nama'];?></h4>

                <p>Anda login sebagai <?php echo $data['level']?>.</p>
              </div>
          <div class="box box-solid box-success">
            <div class="box-header with-border">
              <h3 class="box-title">HALAMAN UTAMA!!!</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                  <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                  <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                  <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
                </ol>
                <div class="carousel-inner">
                  <div class="item active">
                    <img src="img/slide1.jpg" alt="First slide">

                    <div class="carousel-caption">
                      
                    </div>
                  </div>
                  <div class="item">
                    <img src="img/slide2.jpg" alt="Second slide">

                    <div class="carousel-caption">
                      
                    </div>
                  </div>
                  <div class="item">
                    <img src="img/slide3.jpg" alt="Third slide">

                    <div class="carousel-caption">
                 
                    </div>
                  </div>
                </div>
                <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                  <span class="fa fa-angle-left"></span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                  <span class="fa fa-angle-right"></span>
                </a>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><?php echo $jsuratmasuk;?></h3>

              <p style="font-size:13px">Surat Masuk Perminggu</p>
            </div>
            <div class="icon">
              <i class="fa fa-envelope"></i>
            </div>
            <a href="?page=suratmasuk" class="small-box-footer">Info Selanjutnya <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?php echo $jsuratkeluar;?></h3>

              <p style="font-size:13px">Surat Keluar Perminggu</p>
            </div>
            <div class="icon">
              <i class="fa fa-envelope-o"></i>
            </div>
            <a href="?page=suratkeluar" class="small-box-footer">Info Selanjutnya <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php echo $jdisposisi;?></h3>

              <p>Disposisi</p>
            </div>
            <div class="icon">
              <i class="fa fa-book"></i>
            </div>
            <a href="?page=disposisi" class="small-box-footer">Info Selanjutnya <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-blue">
            <div class="inner">
              <h3><?php echo $jpengguna;?></h3>

              <p>Pengguna</p>
            </div>
            <div class="icon">
              <i class="fa fa-user"></i>
            </div>
            <a href="?page=pengguna" class="small-box-footer">Info Selanjutnya <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
        </div>
        </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
