  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Penduduk
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Penduduk</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">Data Penduduk</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped table-responsive">
                <thead>
                <tr>
                  <th>No</th>
                  <th>NIK</th>
                  <th>Nama</th>
                  <th>Tempat/Tgl Lahir</th>
                  <th>Jenis Kelamin</th>
                  <th>Agama</th>
                  <th>Status Perkawinan</th>
                  <th>Pekerjaan</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
<?php
  $no = 1;
  $sql = $koneksi->query("select * from penduduk");
  while ($data=$sql->fetch_assoc()){
?>
                <tr>
                  <td><?php echo $no++;?></td>
                  <td><?php echo $data['nik'];?></td>
                  <td><?php echo $data['nama'];?></td>
                  <td><?php echo $data['tempatlahir'];?>/<?php echo date('d-m-Y', strtotime($data['tgllahir']))?></td>
                  <td><?php echo $data['jk'];?></td>
                  <td><?php echo $data['agama'];?></td>
                  <td><?php echo $data['statuskawin'];?></td>
                  <td><?php echo $data['pekerjaan'];?></td>
                  <td class="text-center">
                    <a href="?page=penduduk&aksi=edit&id_penduduk=<?php echo $data['id_penduduk'];?>" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></a>
                    <a href="?page=penduduk&aksi=hapus&id_penduduk=<?php echo $data['id_penduduk'];?>" class="btn btn-danger btn-xs hapus" data-toggle="tooltip" data-placement="top" title="Hapus"><i class="fa fa-trash"></i></a>
                    <!-- <a href="?page=suratmasuk&aksi=detail&id_suratmasuk=<?php echo $data['id_suratmasuk'];?>" data-toggle="tooltip" data-placement="top" title="Detail" class="btn btn-warning btn-xs" ><i class="fa fa-pencil-square"></i></a> -->
                  </td>
                </tr>
<?php
  }
?>
                </tbody>
                <tfoot>
                <tr>
                  <th>No</th>
                  <th>NIK</th>
                  <th>Nama</th>
                  <th>Tempat/Tgl Lahir</th>
                  <th>Jenis Kelamin</th>
                  <th>Agama</th>
                  <th>Status Perkawinan</th>
                  <th>Pekerjaan</th>
                  <th>Aksi</th>
                </tr>
                </tfoot>
              </table>
              <button type="button" data-toggle="modal" data-target="#modal-default" class="btn btn-primary"><i class="fa fa-download"></i> Import</button>
              <a href="javascript:history.go(-1)" class="btn btn-default"><i class="fa fa-arrow-left"></i> Kembali</a>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- /.tambah -->
<div class="modal fade " id="modal-default">
  <div class="modal-dialog">
    <div class="modal-content ">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title modal-primary">Cari Data Penduduk</h4>
      </div>
      <div class="modal-body">
        <div class="box box-primary">
            <!-- /.box-header -->
            <div class="box-body">
            <form role="form" action="" method="POST" enctype="multipart/form-data">
            <div class="alert alert-warning alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-warning"></i> Perhatian!</h4>
                Format file yang diimport harus Excel (xlxs, csv, xls)
              </div>
              <div class="form-group">
                  <label for="exampleInputFile">Import File</label>
                  <input type="file" name="excel" accept=".xlsx,.xls,.csv">
                </div>
            </div>
            <!-- /.box-body -->
      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" name="import" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
      </div>
      </form>
<?php
if(isset($_POST["import"])){
  $values = end(explode(".", $_FILES["excel"]["name"])); 
  $format = array("xls", "xlsx", "csv");
  if(in_array($values, $format)) {
    $file = $_FILES["excel"]["tmp_name"]; 
    include("assets/PHPExcel/Classes/PHPExcel/IOFactory.php");
    $objPHPExcel = PHPExcel_IOFactory::load($file);
    // Looping worksheet
    foreach ($objPHPExcel->getWorksheetIterator() as $worksheet){
      $totalrow = $worksheet->getHighestRow();
      for($row=3; $row<=$totalrow; $row++){
        $nik = mysqli_real_escape_string($koneksi, $worksheet->getCellByColumnAndRow(0, $row)->getValue());
        $nama = mysqli_real_escape_string($koneksi, $worksheet->getCellByColumnAndRow(1, $row)->getValue());
        $tempatlahir = mysqli_real_escape_string($koneksi, $worksheet->getCellByColumnAndRow(2, $row)->getValue());
        $tgllahir = mysqli_real_escape_string($koneksi, $worksheet->getCellByColumnAndRow(3, $row)->getValue());
        $jk = mysqli_real_escape_string($koneksi, $worksheet->getCellByColumnAndRow(4, $row)->getValue());
        $dusun = mysqli_real_escape_string($koneksi, $worksheet->getCellByColumnAndRow(5, $row)->getValue());
        $rt = mysqli_real_escape_string($koneksi, $worksheet->getCellByColumnAndRow(6, $row)->getValue());
        $kel = mysqli_real_escape_string($koneksi, $worksheet->getCellByColumnAndRow(7, $row)->getValue());
        $kecamatan = mysqli_real_escape_string($koneksi, $worksheet->getCellByColumnAndRow(8, $row)->getValue());
        $agama = mysqli_real_escape_string($koneksi, $worksheet->getCellByColumnAndRow(9, $row)->getValue());
        $statuskawin = mysqli_real_escape_string($koneksi, $worksheet->getCellByColumnAndRow(10, $row)->getValue());
        $pekerjaan = mysqli_real_escape_string($koneksi, $worksheet->getCellByColumnAndRow(11, $row)->getValue());
        $kwn = mysqli_real_escape_string($koneksi, $worksheet->getCellByColumnAndRow(12, $row)->getValue());
        $sql = $koneksi->query("INSERT INTO penduduk (nik, nama, tempatlahir, tgllahir, jk, dusun, rt, kel, kecamatan, agama, statuskawin, pekerjaan, kwn) 
        VALUES ('".$nik."', '".$nama."','".$tempatlahir."','".$tgllahir."','".$jk."','".$dusun."','".$rt."','".$kel."','".$kecamatan."','".$agama."','".$statuskawin."','".$pekerjaan."','".$kwn."')");
      }

    }
    ?>
    <script>
      swal({
            title: 'Suksess!',
            text: 'Data berhasil diimport',
            type: 'success',
            html: true,
            confirmButtonClass: 'btn-primary',
            confirmButtonText: 'Ooke',
            },function(){
            window.location.href = "?page=penduduk"
          });
    </script>
    <?php
  }else{

    ?>
    <script>
      swal("Gagal!", "Format file harus Excel (xlsx, xls, csv)!", "error");
    </script>
    <?php

  }

}

?>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- /.tambah -->
<script>
  $('.hapus').on('click',function(){
      var getLink = $(this).attr('href');
        swal({
              title: 'Hapus',
              text: 'Apakah anda yakin untuk menghapus data?',
              type: "warning",
              html: true,
              showCancelButton: true,
              confirmButtonClass: 'btn-danger',
              confirmButtonText: 'Hapus!',
              cancelButtonText: "Batal!",
              },function(){
              window.location.href = getLink
          });
      return false;
        });
</script>