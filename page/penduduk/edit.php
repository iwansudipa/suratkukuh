<?php
  $id = $_GET[id_penduduk];
  $sql = $koneksi->query("select * from penduduk where id_penduduk='$id'") or mysqli_error($koneksi);
  $data = $sql->fetch_assoc();

    if (isset($_POST['simpan'])) {
      $nama= strtoupper($_POST['nama']);
      $tempatlahir =  strtoupper($_POST['tempatlahir']);
      $tgllahir=date('Y-m-d', strtotime($_POST['tgllahir']));
      $jk = $_POST['jk'];
      $dusun= ucwords($_POST['dusun']);
      $rt = $_POST['rt'];
      $kel = ucwords($_POST['kel']);
      $kecamatan = ucwords($_POST['kecamatan']);
      $agama = $_POST['agama'];
      $statuskawin = strtoupper($_POST['statuskawin']);
      $pekerjaan = strtoupper($_POST['pekerjaan']);
      $kwn =strtoupper($_POST['kwn']);

      $sql = $koneksi->query("update penduduk set nama='$nama', tempatlahir='$tempatlahir', tgllahir='$tgllahir', jk='$jk', dusun='$dusun', rt='$rt', kel='$kel',
      kecamatan='$kecamatan', agama='$agama', statuskawin='$statuskawin', pekerjaan='$pekerjaan', kwn='$kwn' where id_penduduk='$id' ") or die(mysqli_error($koneksi));
      if ($sql==true) {
        ?>
        <script>
            swal({
                title: 'Suksess!',
                text: 'Data Berhasil Diedit',
                type: 'success',
                html: true,
                confirmButtonClass: 'btn-primary',
                confirmButtonText: 'Ooke',
                },function(){
                window.location.href = "?page=penduduk"
                });
        </script>
        <?php
      }
    }   
?>  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Penduduk
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="?page=penduduk">Penduduk</a></li>
        <li class="active">Edit</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Data Penduduk</h3>
            </div>
            <form role="form" method="POST">
              <div class="box-body">
              <div class="row">
              <div class="col-lg-12">
              <div class="alert alert-warning alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-warning"></i> Perhatian!</h4>
                Silahkan ganti isi form dibawah ini untuk mengubah data penduduk yang salah
              </div>
                <div class="text-center">
                    <label style="font-size:18pt;">NIK: <?php echo $data['nik']?></label>
                </div>
              </div>
              <br>
              <br>
              <br>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Nama:</label>
                  <input type="text" name="nama" class="form-control" style="text-transform: uppercase" placeholder="Masukkan Nama" 
                  value="<?php echo $data['nama']?>" required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Tempat Lahir:</label>
                  <input type="text" name="tempatlahir" class="form-control" style="text-transform: uppercase" placeholder="Masukkan Tempat Lahir" 
                  value="<?php echo $data['tempatlahir']?>" required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                <label>Tanggal Lahir:</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" name="tgllahir" class="form-control pull-right" id="datepicker2" placeholder="Masukkan Tanggal Lahir" 
                  value="<?php echo date('d-m-Y', strtotime($data['tgllahir']))?>" required>
                </div>
                <!-- /.input group -->
              </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                <label>Jenis Kelamin:</label>
                <select class="form-control select2" style="width: 100%;" name="jk" required>
                  <option value="">=>Jenis Kelamin<=</option>
                  <option value="Laki-Laki" <?php if ($data['jk']=="Laki-Laki") {
                      echo "selected";
                  }?>>Laki-Laki</option>
                  <option value="PEREMPUAN" <?php if ($data['jk']=="PEREMPUAN") {
                      echo "selected";
                  }?>>PEREMPUAN</option>
                </select>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Dusun:</label>
                  <input type="text" name="dusun" class="form-control" style="text-transform: capitalize" placeholder="Masukkan Dusun" 
                  value="<?php echo $data['dusun']?>" required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>RT/RW:</label>
                  <input type="text" name="rt" class="form-control" placeholder="Masukkan RT/RW" 
                  value="<?php echo $data['rt']?>" required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Kelurahan:</label>
                  <input type="text" name="kel" class="form-control" style="text-transform: capitalize" placeholder="Masukkan Kelurahan" 
                  value="<?php echo $data['kel']?>" required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Kecamatan:</label>
                  <input type="text" name="kecamatan" class="form-control" style="text-transform: capitalize" placeholder="Masukkan Kecamatan" 
                  value="<?php echo $data['kecamatan']?>" required>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                <label>Agama:</label>
                <select class="form-control select2" style="width: 100%;" name="agama" required>
                  <option value="">=>Pilih Agama<=</option>
                  <option value="ISLAM" <?php if ($data[agama]=="ISLAM") {
                      echo "selected";
                  }?>>ISLAM</option>
                  <option value="HINDU" <?php if ($data[agama]=="HINDO") {
                      echo "selected";
                  }?>>HINDU</option>
                  <option value="BUDHA" <?php if ($data[agama]=="BUDHA") {
                      echo "selected";
                  }?>>BUDHA</option>
                  <option value="KRISTEN" <?php if ($data[agama]=="KKRISTEN") {
                      echo "selected";
                  }?>>KRISTEN</option>
                </select>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Status Kawin:</label>
                  <input type="text" name="statuskawin" class="form-control" style="text-transform: uppercase" placeholder="Masukkan Status Kawin" 
                  value="<?php echo $data['statuskawin']?>" required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Pekerjaan:</label>
                  <input type="text" name="pekerjaan" class="form-control" style="text-transform: uppercase" placeholder="Masukkan Pekerjaan" 
                  value="<?php echo $data['pekerjaan']?>" required>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                  <label>Kewarganegaraan:</label>
                  <input type="text" name="kwn" class="form-control" style="text-transform: uppercase" placeholder="Masukkan Kewarganegaraan" 
                  value="<?php echo $data['kwn']?>" required>
                </div>
              </div>
              </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" name="simpan" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                <a href="javascript:history.go(-1)" class="btn btn-default"><i class="fa fa-arrow-left"></i> Kembali</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  