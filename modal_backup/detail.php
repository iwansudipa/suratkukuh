<?php
    include_once 'koneksi.php';
    if($_POST['id']){
        //membuat variabel id berisi post['id']
        $id = $_POST['id'];
        //query standart select where id
        $sql = $koneksi->query("SELECT * FROM suratmasuk WHERE id_suratmasuk='$id'");
        //jika ada datanya
        $data=$sql->fetch_assoc();
    }
?>

        <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">Detail dari Nomor surat masuk <b><?php echo $data['no_suratmasuk'];?></b> </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-striped">
                <tr>
                  <td>Tanggal Surat Masuk :</td>
                  <td><?php echo date('d-M-Y', strtotime($data['tgl_suratmasuk']));?></td>
                </tr>
                <tr>
                  <td>Tanggal Terima Surat :</td>
                  <td><?php echo date('d-M-Y', strtotime($data['tgl_terima']));?></td>
                </tr>
                <tr>
                  <td>Sifat Surat :</td>
                  <td><?php echo $data['sifat'];?></td>
                </tr>
                <tr>
                  <td>Asal Surat Masuk :</td>
                  <td><?php echo $data['asal_suratmasuk'];?></td>
                </tr>
                <tr>
                  <td>Perihal :</td>
                  <td><?php echo $data['perihal'];?></td>
                </tr>
                <tr>
                  <td>Status Disposisi :</td>
                  <td><?php echo $data['status_disposisi'];?></td>
                </tr>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

<div class="modal-footer">
    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary">Save changes</button>
</div>
