  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Surat Masuk
        <small>Pendataan</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Surat Masuk</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">Data Tabel Surat Masuk</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>No Surat</th>
                  <th width="70px">Tgl. Surat</th>
                  <th width="75px">Tgl. Terima</th>
                  <th>Asal</th>
                  <th>Sifat</th>
                  <th>Perihal</th>
                  <th>Status Disposisi</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
<?php
  $no = 1;
  $sql = $koneksi->query("select * from suratmasuk");
  while ($data=$sql->fetch_assoc()){
?>
                <tr>
                  <td><?php echo $no++;?></td>
                  <td><?php echo $data['no_suratmasuk'];?></td>
                  <td><?php echo $data['tgl_suratmasuk'];?></td>
                  <td><?php echo $data['tgl_terima']?></td>
                  <td><?php echo $data['asal_suratmasuk'];?></td>
                  <td><?php echo $data['sifat'];?></td>
                  <td><?php echo $data['perihal'];?></td>
                  <td><?php echo $data['status_disposisi'];?></td>
                  <td class="text-center">
                    <a href="?page=suratmasuk&aksi=edit&id_suratmasuk=<?php echo $data['id_suratmasuk'];?>" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></a>
                    <a href="?page=suratmasuk&aksi=hapus&id_suratmasuk=<?php echo $data['id_suratmasuk'];?>" class="btn btn-danger btn-xs hapus" data-toggle="tooltip" data-placement="top" title="Hapus"><i class="fa fa-trash"></i></a>
                    <div data-toggle="modal" data-target="#modal-default">
                    <button id="<?php echo $data['id_suratmasuk'];?>" data-toggle="tooltip" data-placement="top" title="Detail" class="btn btn-warning btn-xs view_data" ><i class="fa fa-pencil-square"></i></button>
                    </div>
                  </td>
                </tr>
<?php
  }
?>
                </tbody>
                <tfoot>
                <tr>
                  <th>No</th>
                  <th>No Surat</th>
                  <th>Tgl. Surat</th>
                  <th>Tgl. Terima</th>
                  <th>Asal</th>
                  <th>Sifat</th>
                  <th>Perihal</th>
                  <th>Status Disposisi</th>
                  <th>Aksi</th>
                </tr>
                </tfoot>
              </table>
              <a href="?page=suratmasuk&aksi=tambah" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah</a>
              <a href="javascript:history.go(-1)" class="btn btn-default"><i class="fa fa-arrow-left"></i> Kembali</a>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
      </div>
<!-- /.detail -->
      <div class="modal fade" id="modal-default">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Default Modal</h4>
              </div>
              <div class="modal-body" id="data_detail">
			  </div>
              
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
<!-- /.detail -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<script>
  $('.hapus').on('click',function(){
      var getLink = $(this).attr('href');
        swal({
              title: 'Hapus',
              text: 'Apakah anda yakin untuk menghapus data?',
              type: "warning",
              html: true,
              showCancelButton: true,
              confirmButtonClass: 'btn-danger',
              confirmButtonText: 'Hapus!',
              cancelButtonText: "Batal!",
              },function(){
              window.location.href = getLink
          });
      return false;
        });
</script>
<script>
	$(document).ready(function(){
		$('.view_data').click(function(){
			var id = $(this).attr("id");
			
			$.ajax({
				url: 'page/suratmasuk/detail.php',	
				method: 'post',		
				data: {id:id},		
				success:function(data){	
					$('#data_detail').html(data);	
					$('#modal-default').modal("show");	
				}
			});
		});
	});
	</script>